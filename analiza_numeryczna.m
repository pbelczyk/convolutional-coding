%% srypt ten pozwala na uzyskanie teoretycznych wyników zależności BER od
% Eb/No dla wiadomości binarnych przesyłanych przez kanał z addytywnym
% białym szumem Gaussowskim, wynikiem końcowym jest wykres przedstawiający
% te zależności dla dekodowania twardodecyzyjnego, miękkodecyzyjnego oraz
% sytuacji bez kodowania z modulacją BPSK (teoretyczna krzywa BPSK BER). W 
% algorytmie Viterbiego zaimplementowany został algorytm okna przesuwnego 
%(ang. sliding window).

clear all 
close all
clc

fprintf('koder splotowy : sprawność 1/2 długość wymuszona = 3\n\n');


%% parametry kodera

start=1; %początkowa wartość Eb/No w dB (stosunek sygnału do szumu)
stop=10; %końcowa wartość Eb/No w dB (stosunek sygnału do szumu)
step=1; %krok zmiany wartości Eb/No
sim = 500; % liczba różnych bloków wiadomosci dla danej wartości Eb/No

EbNodB = [start:step:stop]; % wartości Eb/N0
EbNodB = fliplr(EbNodB); %odrócenie macierzy lewo -> prawo [1 2 3] ->[3 2 1]

decoding_depth=15; %głębokość dekodowania, długość okna przesuwnego dekodera, optymalna 
%wartość 5*K, K - długość wymuszona kodera

number_of_decoded_bits=3; %liczba bitów przekazywana na wyjśćie dekodera w 
% każdym takcie jego działania; ilość bitów po których okno przesuwne się przesunie
%w procesie dekodowania

LENGTH_cv = 2000;  % długość wiadomości pojedynczego bloku (wejściowych bitów, 
%nie zakodowanych, wchodzących do kodera)

memory_cv = 2;  % ilość komórek pamięci kodera
LENGTH_c_cv = 2*(memory_cv+LENGTH_cv); % długość zakodowanej wiadomości


CodeRate = 1/2; %code rate kodera
BER_HARD = []; %tablica przechowująca BER, dekodowanie twardodecyzyjne
BER_SOFT = []; %tablica przechowująca BER, dekodowanie miękkodecyzyjne

%% Tworzenie macierzy generującej, potrzebna do utworzenia ciągu kodowego
% (zakodowanej wiadomości w koderze splotowym)

 fprintf ('wielomiany generujące:\n g0=[111]  g1=[101]\n');
 g_poly =[1 1 1 0 1 1]; %odpowiedź impulsowa kodera
 %optymalne odczepy na podst. Convolutional Codes
 %with Optimum Distance Spectrum

                         
G_cv = zeros(LENGTH_cv,LENGTH_c_cv); % macierz generująca kodera G

i2 = 1;
for i1=1:LENGTH_cv
    % uzupełnianie macierzy generującej odpowiedzią impulsową
    G_cv(i1,i2:i2+length(g_poly)-1) = g_poly;
    i2 = i2 + memory_cv;
end
G_cv;
length(G_cv);


%% pętle po Eb/No oraz sim
for snr=1:length(EbNodB) %pętla, różne wartości Eb/No

fprintf('\n ----------\n');
fprintf('\nEbNo : %f\n',EbNodB(snr));

total_VB_Err_HARD = 0; %l błędó we wszystkich blokach hard dla danego Eb/No
total_VB_Err = 0; % l błędów we wszystkich blokach soft dla danego Eb/No


for block=1:sim % sim - ilość bloków, różnych wiadomości generowanych dla danego Eb/No
    
%% Kodowanie
m_cv = rand(1,LENGTH_cv)>0.5;% generowanie randomowej wiadomości binarnej, parametry 
%rand (l. wierszy, l. kolumn); >0.5 sprawdza czy wygenerowane liczby są większe 
%od 0.5 i wtedy zamienia to na 1 a jak mniejsze to 0


% m_cv = dlmread('wejscie_do_kodera.dat'); %możliwość wczytania wiadomości
% wejściowej z pliku
% length(m_cv);

c_cv = mod(m_cv*G_cv,2); %kodowanie przy użyciu macierzy generującej
% dlmwrite('wyjscie_kodera.dat',c_cv,'delimiter','\n'); %zapis zakodowanej wiadomości


T = (2*c_cv)-1; %modulacja BPSK


%% kanał AWGN
% w kanale AWGN sygnał jest uszkadzany poprzez addytywny szum o mocy
% widmowej No/2 [Watt/Hz], wariancja sigma^2 = No/2


% bez kodowania Es/No = Eb/No (jeden symbol kanału na bit)
% dla kanału z kodowaniem Es/No = Eb/No + 10log(k/n)
EsNodB=EbNodB(snr)+10*log10(CodeRate); %przeliczenie na Es/No
EsNo=10^(EsNodB/10); % przelicznenie na liniową skale


Es=1;
No=Es/EsNo; % obliczenie mocy szumu No

sigma2=No/2; % wariancja szumu - jego średnia moc
sigma=sqrt(sigma2);
%sigma=0;



% Generowanie szumu

noise=zeros(1,LENGTH_c_cv);   % AWGN

noise=sigma*randn(1,LENGTH_c_cv);
temp1=std(noise); % odchylenie standardowe z wektora szumu
while (temp1 >= (sigma+0.01)) | (temp1 <= (sigma-0.01))%odchylenie standardowe z przedziału
    % sigma+-0.01; jeśli nie jest z tego przedziału to generuje szum
    % dopóki się w tym przedziale zmieści odchylenie standardowe
    noise=sigma*randn(1,LENGTH_c_cv);
    temp1=std(noise);
end

%fprintf('\n szum:');
% noise;


%fprintf('zakodowana wiadomość po modulacji bpsk z addytywnym szumem:\n');
R = T + noise;%otrzymany sygnał po modulacji bpsk z addyttywnym szumem



% fprintf('\nwiadomość po twardodecyzyjnym sprawdzaniu znaku zakodowanej wiadomości\n');
% fprintf('po modulacji bpsk z szumem\n');
cbar = R>0;%twardodecyzyjne sprawdzenie znaku, jeśli R>0 to 1, jeśli R<0 to -1

%fprintf('liczba błędów dla wiadomości po detekcji znaku:\n');
%number_of_error = sum(abs(cbar - c_cv))

%%
max_wartosc = max(R); 
min_wartosc = min(R);
srodek = max_wartosc-(max_wartosc-min_wartosc)/2;


%% kwantyzacja sygnału z szumem

%============================== rownomierna z nie przesunietym offsetem
center = 0;
part2 = max_wartosc/4; %wartosc max probki/4
partition = [center-3*part2, center-2*part2, center-part2, center, center+part2, center+2*part2, center+3*part2];
codebook = [-1, -.75, -.5, -.25, .25, .5, .75, 1];

%%
[index, quants]=quantiz(R,partition,codebook);
%  dlmwrite('quant.dat',quants,'delimiter',' ');

% quants = dlmread('quant.dat');


%% algorytm Viterbiego twardodecyzyjny
N = LENGTH_cv+memory_cv+1;% N = Length_cv + memory_cv +1
% For a 15-bit message with two encoder memory flushing bits, there are 17 time instants in addition 
%to t = 0, which represents the initial condition of the encoder
%N -1 to liczba kroków kodowania wiadomości od zerowego rejestru na początku
%do zerowego rejestru na koniec (zera w kom pamieci kodera)

m_av_hard = zeros (1,LENGTH_cv+memory_cv); % wiadomosc odkodowana, bedzie 
%taka ile kroków kodowania czyli o memory_cv za duża

NS = zeros (4,3); % tablica przejśc do następnych stanów "next state table", pierwsza kolumna to 
%current state, druga kolumna to next state if input = '0', trzecia if
%input ='1'

NS(1,1) = 1; % 1 <-- stan '00'
NS(1,2) = 1;
NS(2,2)=1;
NS(2,1)= 2; NS(3,2)=2; NS(4,2) = 2; % 2<-- stan '01'
NS(1,3)=3; NS(2,3)=3; NS(3,1) = 3; % 3 <-- stan '10'
NS(3,3)=4; NS(4,1)=4; NS(4,3) = 4;


% OS tablica wyjściowych symboli kolejno wiersz 1 to stan 1, wiersz 2 stan 2,
% kolumna 1 i2 wejsciowy symbol '0', kolumna 3 i 4 wejsciowy symbol '1'
OS = [0 0 1 1; 1 1 0 0; 1 0 0 1; 0 1 1 0];% tablica wyjsciowych symboli


CS_NS = zeros(4,4);% tablica pokazująca jaki bit spowodował przejscie z current state do next state
%wiersze to kolejne current state, kolumny kolejne next state, pola to bity
%input
CS_NS(1,1)=0; CS_NS(1,3)=1;
CS_NS(2,1)=0; CS_NS(2,3)=1;
CS_NS(3,2)=0; CS_NS(3,4)=1;
CS_NS(4,2)=0; CS_NS(4,4)=1;


n=2; % z code rate ile bitów generowanych w 1 takcie
    
% ------------------------------------------------------------------------------------------
 %% Viterbi twardodecyzyjny
%% pierwsza faza działania alg Viterbiego, obliczenie metryk dojścia do
 % wszystkich możliwych stanów wykresu kratowego, algorytm zaczyna działanie
 %w stanie '00', stany definiowane zawartością komórek pamięci algorytm Viterbiego rózni
 % sie od stosowanego w fazie regularnej; w pierwszej fazie ścieżki na
 % wykresie kratowym muszą 'rozrosnąć' sie do kazdego możliwego stanu

 N1=number_of_decoded_bits+decoding_depth+1; %N1-1=number_of_decoded_bits+decoding_depth
 %N1-1 jest to ilosc taktow po ktorych zdekoduje czesc ciagu nadanego, N1-1 = N+D
 % gdzie N to ilosc bitów które bedą odkodowane (number_of_decoded_bits), D (decoding_depth) =
 % 3 do 5*K gdzie K to długosc wymuszona, wszystkie ścieżki przetrwania po zdekodowaniu odpowiednio 
 %duzego bloku danych pokrywaja sie na początku
 
 SS = zeros (1,N1); % state sequence, sekwencja stanów przez jakie przechodził koder
 %kodując wiadomość wejściową
AEM = zeros (4,N1); %accumulated error metrics, tablica skumulowanych metryk błędów
SPH = zeros (4,N1);% state predecessor history, historia poprzednich stanów
 
 i4=1; %licznik taktów dekodera

% sprawdzanie metryk dochodzących do stanu 00
d1 = 0;
    if cbar(2*i4-1) ~= 100 %sprawdza czy nie jest to symbol dodany przez depuncturing
        %puncturing nie używany w teoretycznych wykresach, tylko w
        %laboratorium
        d1 = OS(1,1) ~= cbar(2*i4-1);%obliczanie odległosci Hamminga będacej metryka
        %częściową gałęzi
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d1 = d1 + (OS(1,2) ~= cbar(2*i4-1+n-1));%obliczanie odległosci Hamminga będacej 
        % całościową metryką gałęzi
    end
 % sprawdzanie metryk dochodzących do stanu 10   
 d5=0;
 
    if cbar(2*i4-1) ~= 100
        d5 = OS(1,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d5 = d5 + (OS(1,4) ~= cbar(2*i4-1+n-1));
    end
%=================
%             if AEM(1,i4)+d1<AEM(1,i4)+d5
                SPH(1,i4)=1;%zapis do tablicy survivor path history z którego stanu
                %przeszedł algorytm w danym takcie do danego stanu; stan 1
                %-> '00' 2 -> '01' 3 -> '10' 4 -> '11'
                AEM(1,i4+1)=AEM(1,i4)+d1;%zapis do tablicy accumulated error metric
                %skumulowanej metryki przejscia związanej z daną gałęzią
%             else
                SPH(3,i4)=1;
                AEM(3,i4+1)=AEM(1,i4)+d5;
%             end
            
i4=2;


% sprawdzanie metryk dochodzących do stanu 00
d1 = 0;
    if cbar(2*i4-1) ~= 100
        d1 = OS(1,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d1 = d1 + (OS(1,2) ~= cbar(2*i4-1+n-1));
    end
% sprawdzanie metryk dochodzących do stanu 10    
 d5=0;
 
    if cbar(2*i4-1) ~= 100
        d5 = OS(1,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d5 = d5 + (OS(1,4) ~= cbar(2*i4-1+n-1));
    end
%================

%             if AEM(1,i4)+d1<AEM(1,i4)+d5
                SPH(1,i4)=1;
                AEM(1,i4+1)=AEM(1,i4)+d1;
%             else
                SPH(3,i4)=1;
                AEM(3,i4+1)=AEM(1,i4)+d5;
%             end  


% sprawdzanie metryk dochodzących do stanu 01
d3=0;
    if cbar(2*i4-1) ~= 100
        d3 = OS(3,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d3 = d3 + (OS(3,2) ~= cbar(2*i4-1+n-1));
    end
% sprawdzanie metryk dochodzących do stanu 11
d7=0;
    if cbar(2*i4-1) ~= 100
        d7 = OS(3,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d7 = d7 + (OS(3,4) ~= cbar(2*i4-1+n-1));
    end
%================
    
%                if AEM(3,i4)+d3<AEM(3,i4)+d7
                 SPH(2,i4)=3;
                 AEM(2,i4+1)=AEM(3,i4)+d3;
%                else
                 SPH(4,i4)=3;
                 AEM(4,i4+1)=AEM(3,i4)+d7;
%                end
               %AEM(1,i4+1)
 
%% faza regularna alg Viterbiego, pętla do wyliczania metryk dojścia do
% wszystkich stanów, w tej fazie z kazdego stanu mozna dojśc do dwóch
% mozliwych stanów w takcie nastepnym
%tablice AEM, SPH uzupełniane przez (number_of_decoded_bits+5*K)taktów
 % długość = sliding windows + number_of_decoded_bits
 
 for i4=3:N1-1%licznik do wpisywania w kolumny tablic
     
% sprawdzanie metryk dochodzących do stanu 00
d1 = 0;% różnice między zakodowanym 11 a sekwencja wyjsciowa kodera
%              % dla przejścia do stanu 00 ze stanu 00
    if cbar(2*i4-1) ~= 100
        d1 = OS(1,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d1 = d1 + (OS(1,2) ~= cbar(2*i4-1+n-1));
    end
  
d2 = 0;% różnice między zakodowanym 11 a sekwencja wyjsciowa kodera
%              % dla przejścia do stanu 00 ze stanu 01
    if cbar(2*i4-1) ~= 100
        d2 = OS(2,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d2 = d2 + (OS(2,2) ~= cbar(2*i4-1+n-1));
    end
%===================     

             if AEM(1,i4)+d1<AEM(2,i4)+d2
                  SPH(1,i4)=1;%przeszliśmy do stanu '00' ze stanu '00'
                  AEM(1,i4+1)=AEM(1,i4)+d1;
             else
                 SPH(1,i4)=2;% przeszlismy do stanu 00 ze stanu 01
                 AEM(1,i4+1)=AEM(2,i4)+d2;
             end
             
% sprawdzanie metryk dochodzących do stanu 01
d3=0;
    if cbar(2*i4-1) ~= 100
        d3 = OS(3,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d3 = d3 + (OS(3,2) ~= cbar(2*i4-1+n-1));
    end
    
d4=0;
    if cbar(2*i4-1) ~= 100
        d4 = OS(4,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d4 = d4 + (OS(4,2) ~= cbar(2*i4-1+n-1));
    end
%==================
               if AEM(3,i4)+d3<AEM(4,i4)+d4
                 SPH(2,i4)=3;
                 AEM(2,i4+1)=AEM(3,i4)+d3;
               else
                 SPH(2,i4)=4;
                 AEM(2,i4+1)=AEM(4,i4)+d4;
               end
               
% sprawdzanie metryk dochodzących do stanu 10
d5=0;
    if cbar(2*i4-1) ~= 100
        d5 = OS(1,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d5 = d5 + (OS(1,4) ~= cbar(2*i4-1+n-1));
    end
    
d6=0;
    if cbar(2*i4-1) ~= 100
        d6 = OS(2,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d6 = d6 + (OS(2,4) ~= cbar(2*i4-1+n-1));
    end
%===================
             if AEM(1,i4)+d5<AEM(2,i4)+d6
               SPH(3,i4)=1;
               AEM(3,i4+1)=AEM(1,i4)+d5;
             else
               SPH(3,i4)=2;
               AEM(3,i4+1)=AEM(2,i4)+d6;
             end
             
% sprawdzanie metryk dochodzących do stanu 11
d7=0;
    if cbar(2*i4-1) ~= 100
        d7 = OS(3,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d7 = d7 + (OS(3,4) ~= cbar(2*i4-1+n-1));
    end
    
d8=0;
    if cbar(2*i4-1) ~= 100
        d8 = OS(4,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d8 = d8 + (OS(4,4) ~= cbar(2*i4-1+n-1));
    end
%=================
              if AEM(3,i4)+d7<AEM(4,i4)+d8
                  SPH(4,i4)=3;
                  AEM(4,i4+1)=AEM(3,i4)+d7;
              else
                  SPH(4,i4)=4;
                  AEM(4,i4+1)=AEM(4,i4)+d8;
              end
  
 end

 
AEM(:,N1);
[r,c]=find(AEM(:,N1)==min(min(AEM(:,N1)))); % r wskazuje wiersz z min odl hamminga
%na jego podstawie określane jest SPH, najmniejsza metryka wskazuje ścieżke
%na wykresie kratowym

%fprintf('nr stanu z najmniejsza metryka');
r1=r(1,1);

SS(N1)=r1;

% generowanie tablicy stanów przez jakie przechodził algorytm
clearvars i1;

for i1 = (N1-1): -1: 1
SS(i1)=SPH(SS(i1+1),i1); 
end


%obliczanie bitów jakie weszły do kodera na podstawie tablicy przejśc
%pomiedzy stanem current a next
for i1=1:number_of_decoded_bits
   m_av_hard(i1) = CS_NS(SS(i1), SS(i1+1));
    SS(i1);
SS(i1+1);
    
end
%fprintf('\n pierwsza czesc zdekodowanej wiadomosci');
m_av_hard();

%%            koniec dzialania petli do pierwszego dekodowania
%do tego momentu zdekoduje wiadomosc o il bitów = number_of_decoded_bits;
% od tego momentu okno przesuwne bedzie się przesuwac po trellisie i na
% wyjsciu dekodera zwracać number_of_decoded_bits bitów zdekodowanych
% ------------------------------------------------------------------------------------


SPH = circshift(SPH, [0, -number_of_decoded_bits]);%przesuwamy o liczbe odkodowanych bitów, stan na którym sie
%zatrzymalismy przy dekodowaniu nadal zostanie w tablicach i z niego
%ruszymy w następnym kroku trellisa
AEM = circshift(AEM, [0, -number_of_decoded_bits]);
SS = circshift(SS, [0, -number_of_decoded_bits]);


k=0;

krok = 1;
while N1-1+number_of_decoded_bits+k<=N-1 %petla po całej długości zakodowanej wiadomosci

last_aem_column=N1-number_of_decoded_bits; % musi sie wyrownywac po kazdej petli for bo co 3 takty
%ktore obejmuje kazda petla for znow przysowamy macierz
last_sph_column=N1-number_of_decoded_bits-1; %musi sie wyrownywac po kazdej petli for bo co 3 takty
%ktore obejmuje kazda petla for znow przysowamy macierz


 for i4=N1+k:N1-1+number_of_decoded_bits+k%licznik do wpisywania w kolumny tablic

% sprawdzanie metryk dochodzących do stanu 00
d1 = 0;% różnice między zakodowanym 11 a sekwencja wyjsciowa kodera
%              % dla przejścia do stanu 00 ze stanu 00
    if cbar(2*i4-1) ~= 100
        d1 = OS(1,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d1 = d1 + (OS(1,2) ~= cbar(2*i4-1+n-1));
    end
  
d2 = 0;% różnice między zakodowanym 11 a sekwencja wyjsciowa kodera
%              % dla przejścia do stanu 00 ze stanu 01
    if cbar(2*i4-1) ~= 100
        d2 = OS(2,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d2 = d2 + (OS(2,2) ~= cbar(2*i4-1+n-1));
    end
%===================     

             if AEM(1,last_aem_column)+d1<AEM(2,last_aem_column)+d2
                  SPH(1,last_aem_column)=1;%przeszliśmy do stanu '00' ze stanu '00'
                  AEM(1,last_aem_column+1)=AEM(1,last_aem_column)+d1;
             else
                 SPH(1,last_aem_column)=2;% przeszlismy do stanu 00 ze stanu 01
                 AEM(1,last_aem_column+1)=AEM(2,last_aem_column)+d2;
             end

% sprawdzanie metryk dochodzących do stanu 01
d3=0;
    if cbar(2*i4-1) ~= 100
        d3 = OS(3,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d3 = d3 + (OS(3,2) ~= cbar(2*i4-1+n-1));
    end
    
d4=0;
    if cbar(2*i4-1) ~= 100
        d4 = OS(4,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d4 = d4 + (OS(4,2) ~= cbar(2*i4-1+n-1));
    end
%==================
               if AEM(3,last_aem_column)+d3<AEM(4,last_aem_column)+d4
                 SPH(2,last_aem_column)=3;
                 AEM(2,last_aem_column+1)=AEM(3,last_aem_column)+d3;
               else
                 SPH(2,last_aem_column)=4;
                 AEM(2,last_aem_column+1)=AEM(4,last_aem_column)+d4;
               end

% sprawdzanie metryk dochodzących do stanu 10
d5=0;
    if cbar(2*i4-1) ~= 100
        d5 = OS(1,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d5 = d5 + (OS(1,4) ~= cbar(2*i4-1+n-1));
    end
    
d6=0;
    if cbar(2*i4-1) ~= 100
        d6 = OS(2,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d6 = d6 + (OS(2,4) ~= cbar(2*i4-1+n-1));
    end
%===================
             if AEM(1,last_aem_column)+d5<AEM(2,last_aem_column)+d6
               SPH(3,last_aem_column)=1;
               AEM(3,last_aem_column+1)=AEM(1,last_aem_column)+d5;
             else
               SPH(3,last_aem_column)=2;
               AEM(3,last_aem_column+1)=AEM(2,last_aem_column)+d6;
             end

% sprawdzanie metryk dochodzących do stanu 11
d7=0;
    if cbar(2*i4-1) ~= 100
        d7 = OS(3,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d7 = d7 + (OS(3,4) ~= cbar(2*i4-1+n-1));
    end
    
d8=0;
    if cbar(2*i4-1) ~= 100
        d8 = OS(4,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d8 = d8 + (OS(4,4) ~= cbar(2*i4-1+n-1));
    end
%=================
              if AEM(3,last_aem_column)+d7<AEM(4,last_aem_column)+d8
                  SPH(4,last_aem_column)=3;
                  AEM(4,last_aem_column+1)=AEM(3,last_aem_column)+d7;
              else
                  SPH(4,last_aem_column)=4;
                  AEM(4,last_aem_column+1)=AEM(4,last_aem_column)+d8;
              end
  
   last_aem_column=last_aem_column+1;
   last_sph_column=last_sph_column+1;
 end  
 
    %fprintf ('\nSurvivor path metrics are:\n'); 
   % fprintf('\n\n\nSPH z kroku petli numer');
    krok;
SPH;
%fprintf ('\nAccumulated error metrics are:\n');
AEM;
 
clearvars SS
% fprintf('alst_aem za forem');
last_aem_column;
AEM(:,last_aem_column);
[r,c]=find(AEM(:,last_aem_column)==min(min(AEM(:,last_aem_column)))); % r wskazuje wiersz z min odl hamminga
r1=r(1,1);

SS(last_aem_column)=r1;
% generowanie tablicy stanów przez jakie przechodził algorytm
for i1=last_aem_column -1: -1: 1 %SS obliczane od konca trellis tego do samego początku
i1;
SS(i1)=SPH(SS(i1+1),i1);

end
SS;

%obliczanie bitów jakie weszły do kodera na podstawie tablicy przejśc
%pomiedzy stanem current a next
for i1=number_of_decoded_bits+1+k:2*number_of_decoded_bits+k
   m_av_hard(i1) = CS_NS(SS(i1-k-number_of_decoded_bits), SS(i1-k-number_of_decoded_bits+1));
   SS(i1-k-number_of_decoded_bits);
SS(i1-k-number_of_decoded_bits+1);
    
end
%fprintf('zdekodowana wiadomosc w kroku petli while');

% m_av_hard();

%przesuwanie tablic
AEM = circshift(AEM, [0, -number_of_decoded_bits]);
SPH = circshift(SPH, [0, -number_of_decoded_bits]);
SS = circshift(SS, [0, -number_of_decoded_bits]);


k=k+number_of_decoded_bits;
krok=krok+1;

end


licznik=number_of_decoded_bits+1+k;
licznik2=number_of_decoded_bits+1+k+decoding_depth-1;
k;
for i1=number_of_decoded_bits+1+k:number_of_decoded_bits+1+k+decoding_depth-1
   m_av_hard(i1) = CS_NS(SS(i1-k-number_of_decoded_bits), SS(i1-k-number_of_decoded_bits+1));
end
m_av_hard ;
length(m_av_hard);
%%

%wiadomość odkodowana bez pustych znaków z konca, (są zakodowane same zera
%przez koder podczas wypróżniania pamięci kodera)
m_av_hard1 = m_av_hard(1:LENGTH_cv);% do length_cv ignorowanie pustych znaków
% dlmwrite('wyjscie_dekodera.dat',m_av_hard1,'delimiter',' ');

VB_Err_HARD = sum(abs(m_av_hard1-m_cv)); % liczba błędów w pojedynczym bloku

%fprintf('\n liczba błędów we wszystkich blokach hard viterbi');
total_VB_Err_HARD = total_VB_Err_HARD + VB_Err_HARD; % liczba wszystkich błędów wystpujących w ilosci blokow
% = sim, dla konkretnej wartosci EbNo


%% Viterbi soft decision


m_av_soft = zeros (1,1);%LENGTH_cv+memory_cv); % wiadomosc odkodowana, bedzie taka ile kroków kodowania czyli o 
%memory_cv za duża


% OS tablica wyjściowych symboli kolejno wiersz 1 to stan 1, wiersz 2 stan 2,
% kolumna 1 i2 wejsciowy symbol '0', kolumna 3 i 4 wejsciowy symbol '1'
OS = [-1 -1 1 1; 1 1 -1 -1; 1 -1 -1 1; -1 1 1 -1];%tablica wyjsciowych symboli wartości -1 zamiast 0 w stosunku
%do hard viterbi bo wektor do porównania z OS to R który jest bpsk + szum

 SS = zeros (1,N); % state sequence
AEM = zeros (4,N); %accumylated error metrics
SPH = zeros (4,N);% state predecessor history, historia poprzednich stanów
   
 %% pierwsza faza działania alg Viterbiego, obliczenie metryk dojścia do
 % wszystkich możliwych stanów wykresu kratowego, algorytm zaczyna działanie
 %w stanie '00', stany definiowane zawartością komórek pamięci algorytm Viterbiego rózni
 % sie od stosowanego w fazie regularnej; w pierwszej fazie ścieżki na
 % wykresie kratowym muszą 'rozrosnąć' sie do kazdego możliwego stanu
 
 i4=1;
% sprawdzanie metryk dochodzących do stanu 00
d1 = 0;
    if quants(2*i4-1) ~= 100
        d1 = (abs( quants(2*i4-1) - OS(1,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d1 = d1 + (abs( quants(2*i4-1+n-1) - OS(1,2) ))^2;
    end
 % sprawdzanie metryk dochodzących do stanu 10   
 d5=0;
 
    if quants(2*i4-1) ~= 100
        d5 = (abs( quants(2*i4-1) - OS(1,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d5 = d5 + (abs( quants(2*i4-1+n-1) - OS(1,4) ))^2;
    end
%=================
%             if AEM(1,i4)+d1<AEM(1,i4)+d5
                SPH(1,i4)=1;
                AEM(1,i4+1)=AEM(1,i4)+d1;
%             else
                SPH(3,i4)=1;
                AEM(3,i4+1)=AEM(1,i4)+d5;
%             end
      
i4=2;

% sprawdzanie metryk dochodzących do stanu 00
d1 = 0;
    if quants(2*i4-1) ~= 100
        d1 = (abs( quants(2*i4-1) - OS(1,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d1 = d1 + (abs( quants(2*i4-1+n-1) - OS(1,2) ))^2;
    end
 % sprawdzanie metryk dochodzących do stanu 10   
 d5=0;
 
    if quants(2*i4-1) ~= 100
        d5 = (abs( quants(2*i4-1) - OS(1,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d5 = d5 + (abs( quants(2*i4-1+n-1) - OS(1,4) ))^2;
    end
%=================
        
%             if AEM(1,i4)+d1<AEM(1,i4)+d5
                SPH(1,i4)=1;
                AEM(1,i4+1)=AEM(1,i4)+d1;
%             else
                SPH(3,i4)=1;
                AEM(3,i4+1)=AEM(1,i4)+d5;
%             end
            
% sprawdzanie metryk dochodzących do stanu 00
d3 = 0;
    if quants(2*i4-1) ~= 100
        d3 = (abs( quants(2*i4-1) - OS(3,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d3 = d3 + (abs( quants(2*i4-1+n-1) - OS(3,2) ))^2;
    end
 % sprawdzanie metryk dochodzących do stanu 10   
 d7=0;
 
    if quants(2*i4-1) ~= 100
        d7 = (abs( quants(2*i4-1) - OS(3,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d7 = d7 + (abs( quants(2*i4-1+n-1) - OS(3,4) ))^2;
    end
%=================
%                if AEM(3,i4)+d3<AEM(3,i4)+d7
                 SPH(2,i4)=3;
                 AEM(2,i4+1)=AEM(3,i4)+d3;
%                else
                 SPH(4,i4)=3;
                 AEM(4,i4+1)=AEM(3,i4)+d7;
%                end
               %AEM(1,i4+1)
   
%% faza regularna alg Viterbiego, pętla do wyliczania metryk dojścia do
% wszystkich stanów, w tej fazie z kazdego stanu mozna dojśc do dwóch
% mozliwych stanów w takcie nastepnym
%tablice AEM, SPH uzupełniane przez (number_of_decoded_bits+5*K)taktów
 % długość = sliding windows + number_of_decoded_bits
 
 for i4=3:N1-1%licznik do wpisywania w kolumny tablic
     i4;
     
% sprawdzanie metryk dochodzących do stanu 00
d1 = 0;% różnice między zakodowanym 11 a sekwencja wyjsciowa kodera
%              % dla przejścia do stanu 00 ze stanu 00
    if quants(2*i4-1) ~= 100
        d1 = (abs( quants(2*i4-1) - OS(1,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d1 = d1 + (abs( quants(2*i4-1+n-1) - OS(1,2) ))^2;
    end
   
 d2=0;% różnice między zakodowanym 11 a sekwencja wyjsciowa kodera
%              % dla przejścia do stanu 00 ze stanu 01
 
    if quants(2*i4-1) ~= 100
        d2 = (abs( quants(2*i4-1) - OS(2,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d2 = d2 + (abs( quants(2*i4-1+n-1) - OS(2,2) ))^2;
    end
%=================
             if AEM(1,i4)+d1<AEM(2,i4)+d2
                  SPH(1,i4)=1;%przeszliśmy do stanu '00' ze stanu '00'
                  AEM(1,i4+1)=AEM(1,i4)+d1;
             else
                 SPH(1,i4)=2;% przeszlismy do stanu 00 ze stanu 01
                 AEM(1,i4+1)=AEM(2,i4)+d2;
             end
             
% sprawdzanie metryk dochodzących do stanu 01
d3 = 0;

    if quants(2*i4-1) ~= 100
        d3 = (abs( quants(2*i4-1) - OS(3,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d3 = d3 + (abs( quants(2*i4-1+n-1) - OS(3,2) ))^2;
    end
   
d4=0;
 
    if quants(2*i4-1) ~= 100
        d4 = (abs( quants(2*i4-1) - OS(4,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d4 = d4 + (abs( quants(2*i4-1+n-1) - OS(4,2) ))^2;
    end
%=================
               if AEM(3,i4)+d3<AEM(4,i4)+d4
                 SPH(2,i4)=3;
                 AEM(2,i4+1)=AEM(3,i4)+d3;
               else
                 SPH(2,i4)=4;
                 AEM(2,i4+1)=AEM(4,i4)+d4;
               end

% sprawdzanie metryk dochodzących do stanu 10 
d5 = 0;

    if quants(2*i4-1) ~= 100
        d5 = (abs( quants(2*i4-1) - OS(1,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d5 = d5 + (abs( quants(2*i4-1+n-1) - OS(1,4) ))^2;
    end
   
d6=0;
 
    if quants(2*i4-1) ~= 100
        d6 = (abs( quants(2*i4-1) - OS(2,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d6 = d6 + (abs( quants(2*i4-1+n-1) - OS(2,4) ))^2;
    end
%=================
             if AEM(1,i4)+d5<AEM(2,i4)+d6
               SPH(3,i4)=1;
               AEM(3,i4+1)=AEM(1,i4)+d5;
             else
               SPH(3,i4)=2;
               AEM(3,i4+1)=AEM(2,i4)+d6;
             end
             
% sprawdzanie metryk dochodzących do stanu 11
d7 = 0;

    if quants(2*i4-1) ~= 100
        d7 = (abs( quants(2*i4-1) - OS(3,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d7 = d7 + (abs( quants(2*i4-1+n-1) - OS(3,4) ))^2;
    end
   
d8=0;
 
    if quants(2*i4-1) ~= 100
        d8 = (abs( quants(2*i4-1) - OS(4,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d8 = d8 + (abs( quants(2*i4-1+n-1) - OS(4,4) ))^2;
    end
%=================
              if AEM(3,i4)+d7<AEM(4,i4)+d8
                  SPH(4,i4)=3;
                  AEM(4,i4+1)=AEM(3,i4)+d7;
              else
                  SPH(4,i4)=4;
                  AEM(4,i4+1)=AEM(4,i4)+d8;
              end
  
end 

AEM(:,N1);
[r,c]=find(AEM(:,N1)==min(min(AEM(:,N1)))); % r wskazuje wiersz z min odl hamminga
%fprintf('nr stanu z najmniejsza metryka');

r1=r(1,1);

SS(N1)=r1;
%SPH(SS(18),17)
% generowanie tablicy stanów przez jakie przechodził algorytm
clearvars i1;

for i1 = (N1-1): -1: 1

SS(i1)=SPH(SS(i1+1),i1); 

end

SS;
SPH;
AEM;
CS_NS;

%obliczanie bitów jakie weszły do kodera na podstawie tablicy przejśc
%pomiedzy stanem current a next
for i1=1:number_of_decoded_bits
   m_av_soft(i1) = CS_NS(SS(i1), SS(i1+1));
    SS(i1);
SS(i1+1);
    
end
%fprintf('\n pierwsza czesc zdekodowanej wiadomosci');
m_av_soft();


%%            koniec dzialania petli do pierwszego dekodowania
%do tego momentu zdekoduje wiadomosc o il bitów = number_of_decoded_bits;
% od tego momentu okno przesuwne bedzie się przesuwac po trellisie i na
% wyjsciu dekodera zwracać number_of_decoded_bits bitów zdekodowanych
% -------------------------------------------------------------------------------------


%fprintf('\n po przesunieciu tablic');

SPH = circshift(SPH, [0, -number_of_decoded_bits]);%przesuwamy o liczbe odkodowanych bitów, stan na którym sie
%zatrzymalismy przy dekodowaniu nadal zostanie w tablicach i z niego
%ruszymy w następnym kroku trellisa
AEM = circshift(AEM, [0, -number_of_decoded_bits]);
SS = circshift(SS, [0, -number_of_decoded_bits]);


% N1-1;
k=0;

krok = 1;

while N1-1+number_of_decoded_bits+k<=N-1 %tyle kroków kodowania jest dla wiadomosci 17 bitowej

last_aem_column=N1-number_of_decoded_bits; %=5 musi sie wyrownywac po kazdej petli for bo co 3 takty
%ktore obejmuje kazda petla for znow przysowamy macierz
last_sph_column=N1-number_of_decoded_bits-1; %=4 musi sie wyrownywac po kazdej petli for bo co 3 takty
%ktore obejmuje kazda petla for znow przysowamy macierz


 for i4=N1+k:N1-1+number_of_decoded_bits+k%licznik do wpisywania w kolumny tablic

% sprawdzanie metryk dochodzących do stanu 00
d1 = 0;% różnice między zakodowanym 11 a sekwencja wyjsciowa kodera
%              % dla przejścia do stanu 00 ze stanu 00
    if quants(2*i4-1) ~= 100
        d1 = (abs( quants(2*i4-1) - OS(1,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d1 = d1 + (abs( quants(2*i4-1+n-1) - OS(1,2) ))^2;
    end
   
 d2=0;% różnice między zakodowanym 11 a sekwencja wyjsciowa kodera
%              % dla przejścia do stanu 00 ze stanu 01
 
    if quants(2*i4-1) ~= 100
        d2 = (abs( quants(2*i4-1) - OS(2,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d2 = d2 + (abs( quants(2*i4-1+n-1) - OS(2,2) ))^2;
    end
%=================
             if AEM(1,last_aem_column)+d1<AEM(2,last_aem_column)+d2
                  SPH(1,last_aem_column)=1;%przeszliśmy do stanu '00' ze stanu '00'
                  AEM(1,last_aem_column+1)=AEM(1,last_aem_column)+d1;
             else
                 SPH(1,last_aem_column)=2;% przeszlismy do stanu 00 ze stanu 01
                 AEM(1,last_aem_column+1)=AEM(2,last_aem_column)+d2;
             end

% sprawdzanie metryk dochodzących do stanu 01
d3 = 0;

    if quants(2*i4-1) ~= 100
        d3 = (abs( quants(2*i4-1) - OS(3,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d3 = d3 + (abs( quants(2*i4-1+n-1) - OS(3,2) ))^2;
    end
   
d4=0;
 
    if quants(2*i4-1) ~= 100
        d4 = (abs( quants(2*i4-1) - OS(4,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d4 = d4 + (abs( quants(2*i4-1+n-1) - OS(4,2) ))^2;
    end
%=================
               if AEM(3,last_aem_column)+d3<AEM(4,last_aem_column)+d4
                 SPH(2,last_aem_column)=3;
                 AEM(2,last_aem_column+1)=AEM(3,last_aem_column)+d3;
               else
                 SPH(2,last_aem_column)=4;
                 AEM(2,last_aem_column+1)=AEM(4,last_aem_column)+d4;
               end

% sprawdzanie metryk dochodzących do stanu 10 
d5 = 0;

    if quants(2*i4-1) ~= 100
        d5 = (abs( quants(2*i4-1) - OS(1,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d5 = d5 + (abs( quants(2*i4-1+n-1) - OS(1,4) ))^2;
    end
   
d6=0;
 
    if quants(2*i4-1) ~= 100
        d6 = (abs( quants(2*i4-1) - OS(2,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d6 = d6 + (abs( quants(2*i4-1+n-1) - OS(2,4) ))^2;
    end
%=================
             if AEM(1,last_aem_column)+d5<AEM(2,last_aem_column)+d6
               SPH(3,last_aem_column)=1;
               AEM(3,last_aem_column+1)=AEM(1,last_aem_column)+d5;
             else
               SPH(3,last_aem_column)=2;
               AEM(3,last_aem_column+1)=AEM(2,last_aem_column)+d6;
             end

% sprawdzanie metryk dochodzących do stanu 11
d7 = 0;

    if quants(2*i4-1) ~= 100
        d7 = (abs( quants(2*i4-1) - OS(3,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d7 = d7 + (abs( quants(2*i4-1+n-1) - OS(3,4) ))^2;
    end
   
d8=0;
 
    if quants(2*i4-1) ~= 100
        d8 = (abs( quants(2*i4-1) - OS(4,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d8 = d8 + (abs( quants(2*i4-1+n-1) - OS(4,4) ))^2;
    end
%=================
              if AEM(3,last_aem_column)+d7<AEM(4,last_aem_column)+d8
                  SPH(4,last_aem_column)=3;
                  AEM(4,last_aem_column+1)=AEM(3,last_aem_column)+d7;
              else
                  SPH(4,last_aem_column)=4;
                  AEM(4,last_aem_column+1)=AEM(4,last_aem_column)+d8;
              end
  
   last_aem_column=last_aem_column+1;
   last_sph_column=last_sph_column+1;
   

 end  
 
  %fprintf ('\nSurvivor path metrics are:\n'); 
   % fprintf('\n\n\nSPH z kroku petli numer');
krok;
SPH;
%fprintf ('\nAccumulated error metrics are:\n');
AEM;

% fprintf('alst_aem za forem');
last_aem_column;
AEM(:,last_aem_column);
[r,c]=find(AEM(:,last_aem_column)==min(min(AEM(:,last_aem_column)))); % r wskazuje wiersz z min odl hamminga
r1=r(1,1);

SS(last_aem_column)=r1;
% generowanie tablicy stanów przez jakie przechodził algorytm
for i1=last_aem_column -1: -1: 1 %SS obliczane od konca trellis tego do samego początku
i1;
SS(i1)=SPH(SS(i1+1),i1);
end


%obliczanie bitów jakie weszły do kodera na podstawie tablicy przejśc
%pomiedzy stanem current a next
 for i1=number_of_decoded_bits+1+k:2*number_of_decoded_bits+k
   m_av_soft(i1) = CS_NS(SS(i1-k-number_of_decoded_bits), SS(i1-k-number_of_decoded_bits+1));
%    fprintf('pierwsza') ;
   SS(i1-k-number_of_decoded_bits);
SS(i1-k-number_of_decoded_bits+1); 
end
%fprintf('zdekodowana wiadomosc w kroku petli while');
 
m_av_soft();

%przesuwanie tablic
AEM = circshift(AEM, [0, -number_of_decoded_bits]);
SPH = circshift(SPH, [0, -number_of_decoded_bits]);
SS = circshift(SS, [0, -number_of_decoded_bits]);


k=k+number_of_decoded_bits;
krok=krok+1;

end
%fprintf('\n \n po wyjsciu z while');


%fprintf('do odkodowania po while o dlugosci decoding depth');
licznik=number_of_decoded_bits+1+k;
licznik2=number_of_decoded_bits+1+k+decoding_depth-1;
k;
for i1=number_of_decoded_bits+1+k:number_of_decoded_bits+1+k+decoding_depth-1
   m_av_soft(i1) = CS_NS(SS(i1-k-number_of_decoded_bits), SS(i1-k-number_of_decoded_bits+1));
end
m_av_soft;
length(m_av_soft);
%%

%wiadomość odkodowana bez pustych znaków z konca, (są zakodowane same zera
%przez koder podczas wypróżniania pamięci kodera)
m_av_soft1 = m_av_soft(1:LENGTH_cv);%do length_cv ignorowanie pustych znaków
dlmwrite('wyjscie_dekodera_soft.dat',m_av_soft1,'delimiter',' ');


VB_Err = sum(abs(m_av_soft1-m_cv)); % l błędów w pojedynczym bloku

%fprintf('\n liczba błędów we wszystkich blokach');
total_VB_Err = total_VB_Err + VB_Err; % liczba wszystkich błędów wystpujących w ilosci blokow
% = sim, dla konkretnej wartosci EbNo
end

% calculate bit error rate
BER_HARD = [BER_HARD total_VB_Err_HARD/(LENGTH_cv*block)] %wartości BER dla danych EbNo
BER_SOFT = [BER_SOFT total_VB_Err/(LENGTH_cv*block)]  %wartości BER dla danych EbNo

fprintf('\nSimulated Block = %d \n',block);
fprintf('Decoding error (HARD) = %d \n',total_VB_Err_HARD);
fprintf('Decoding error (SOFT) = %d \n',total_VB_Err);


clear max_wartosc 
clear max(R)
clear min_wartosc 
clear min(R)
clear srodek
clear center
clear part2

end
% fprintf(id,'%18d', number_of_oversampling);
% fprintf(id,'%15d %15d\n', total_VB_Err_HARD,total_VB_Err);
% end
% fclose(id);

%% tworzenie wykresów z wynikami
figure;
semilogy(EbNodB,BER_SOFT,'bs-','LineWidth',2)
set(gca, 'FontSize',14,'FontName','Arial')
hold on;
semilogy(EbNodB,BER_HARD,'ms-','LineWidth',2)

% theory bpsk
BER_BPSK = (1/2)*erfc(sqrt((10.^(EbNodB/10))));
hold on;
semilogy(EbNodB,BER_BPSK,'rs-','LineWidth',2)

grid on;
xlabel('Eb/No (dB)')
ylabel('BER')
title('koder splotowy : sprawność 1/2 długość wymuszona = 3');
legend('miękkodecyzyjny','twardodecyzyjny','BPSK BER','FontSize',12)
set(gcf, 'Position',[1 1 801 620]) %position of figure


