%%skrypt ten pozwala na wygenerowanie randomowej wiadomo�ci binarnej o zadanej
%d�ugo�ci, zakodowanie tej wiadomo�ci za pomoc� kodera splotowego o zadanych
%odczepach ga��zi kodera, punkturyzacje - dzi�ki niej mo�na uzyskiwa�
%r�ne sprawno�ci kodera przy wykorzystaniu tego samego kodera
%macierzystego, mo�liwy jest tak�e przeplot wiadomo�ci zakodowanej zgodnie
%z macierz� przeplotu; za pomoc� generatora generowane s� zakodowane wiadomo�ci
%binarne do wys�ania przez medium transmisyjne i po odebraniu z medium
%przez oscyloskop przeanalizowanie przez jeden z pozosta�ych skrupt�w (np
%zaleznosc_decoding_depth.m


%%
clear all 
close all
clc

fprintf('koder splotowy : sprawno�� 1/2 d�ugo�� wymuszona = 3\n\n');

%% parametry kodera


LENGTH_cv = 4096;  % d�ugo�� wiadomo�ci pojedynczego bloku (wej�ciowych bit�w, 
%nie zakodowanych, wchodz�cych do kodera)
memory_cv = 2;  % ilo�� kom�rek pami�ci kodera,
LENGTH_c_cv = 2*(memory_cv+LENGTH_cv); % d�ugo�� zakodowanej wiadomo�ci



CodeRate = 1/2; %code rate kodera
BER_HARD = []; %tablica przechowuj�ca BER, dekodowanie twardodecyzyjne
BER_SOFT = []; %tablica przechowuj�ca BER, dekodowanie mi�kkodecyzyjne


%% Tworzenie macierzy generuj�cej, potrzebna do utworzenia ci�gu kodowego
% (zakodowanej wiadomo�ci w koderze splotowym)                    
                         
 fprintf ('wielomiany generuj�ce:\n g0=[111]  g1=[101]\n');
 g_poly =[1 1 1 0 1 1]; %odpowied� impulsowa kodera
 %optymalne odczepy na podst. Convolutional Codes
 %with Optimum Distance Spectrum 
            

%fprintf ('wielomiany generuj�ce:\n g0=[101]  g1=[111]\n');
%g_poly = [1 1 0 1 1 1]; % odpowied� impulsowa kodera, tworzona z wielomian�w generuj�cych
                         %     g0 = [1 0 1]
                         %     g1 = [1 1 1]           
 
 
G_cv = zeros(LENGTH_cv,LENGTH_c_cv); % macierz generuj�ca kodera G

i2 = 1;
for i1=1:LENGTH_cv
    % uzupe�nianie macierzy generuj�cej odpowiedzi� impulsow�
    G_cv(i1,i2:i2+length(g_poly)-1) = g_poly;
    i2 = i2 + memory_cv;
end

length(G_cv);
    
%% Kodowanie
m_cv = rand(1,LENGTH_cv)>0.5; % generowanie randomowej wiadomo�ci binarnej, parametry 
%rand (l. wierszy, l. kolumn); >0.5 sprawdza czy wygenerowane liczby s� wi�ksze 
%od 0.5 i wtedy zamienia to na 1 a jak mniejsze to 0


% m_cv = dlmread('wejscie_do_kodera.dat'); %mo�liwo�� wczytania binarnej wiadomo�ci
% wej�ciowej z pliku
% length(m_cv);

dlmwrite('wejscie_do_kodera.dat',m_cv,'delimiter',' '); %zapis wiadomo�ci binarnej do pliku


c_cv = mod(m_cv*G_cv,2); %kodowanie przy u�yciu macierzy generuj�cej
dlmwrite('wyjscie_kodera.dat',c_cv,'delimiter','\n'); %zapis zakodowanej wiadomo�ci


%% puncturing (pozwala zmienia� code rate kodera), wa�ne jest kt�re bity
%s� opuszczane (wi�kszy zysk kodowy), najoptymalniejsze macierze puncturingu 
%wybrane zosta�y do�wiadczalnie

punctured_code = c_cv;

%========= podstawowy 1/2 sprawnosc
% dlmwrite('bez inter bez punc.dat',punctured_code,'delimiter','\n');




%============ punctured matrix do sprawnosci 2/3
% punctured_code (4:4:end)=[];
% dlmwrite('wiadomosc_po_punkturyzacji_2_3.dat',punctured_code,'delimiter','\n');



%============ punctured matrix do sprawnosci 3/4
%  punctured_code (3:3:end)=[];
% dlmwrite('wiadomosc_po_punkturyzacji_3_4.dat',punctured_code,'delimiter','\n');
 


 %============ punctured matrix do sprawnosci 5/6
 punctured_code (3:10:end)=[];
 punctured_code (5:9:end)=[];
 punctured_code (5:8:end)=[];
 punctured_code (6:7:end)=[];
dlmwrite('wiadomosc_po_punkturyzacji_5_6.dat',punctured_code,'delimiter','\n');


% fprintf('dlugosc po puncturingu\n');
 length (punctured_code);
 
%% interleaving (przeplot bitowy), pozwala na eliminacje b��d�w paczkowych
%podczas transmisji

% ============= bez interleavingu
% interleaved_matrix = punctured_code
% length_interleaved_matrix = length(interleaved_matrix);



% ============= przeplot o podanych wymiarach macierzy przeplotu
length_punctured_code = length(punctured_code);% d�ugo�� wiadomo�ci po punkturingu
dlugo1= length_punctured_code +1;
a=164; % l wierszy macierzy przeplotu
b=50; % l kolumn macierzy przeplotu
punctured_code(dlugo1:(a*b))=0; % uzupelnianie wiadomosci z kodera o bity 0 
% tak aby zapelnic ca�a macierz przeplotu o wymiarach a*b; jesli 
% dlugosc wiadomosci rowna sie wymiarom macierzy to nic nie doda
punctured_code;
interleaved_matrix = matintrlv(punctured_code,a,b); % wiadomosc zakodowana po przeplocie
%uzupelnia wiersz po wierszu macierz a odczytuje kolumna po kolumnie
length_interleaved_matrix = length(interleaved_matrix);

dlmwrite('wiadomosc_po_interleavingu.dat',interleaved_matrix,'delimiter','\n'); %zapis
%wiadomo�ci po interleavingu


% =============== przeplot o kwadratowych wymiarach macierzy l wierszy = l kolumn
% length_punctured_code = length(punctured_code);% d�ugo�� wiadomo�ci po punkturingu
% dlugo1= length_punctured_code +1;
% interleaved_matrix_dimension = ceil(sqrt(length(punctured_code)))
% punctured_code(dlugo1:((interleaved_matrix_dimension)^2))=0;
% punctured_code
% interleaved_matrix = matintrlv(punctured_code,interleaved_matrix_dimension,interleaved_matrix_dimension)
% length_interleaved_matrix = length(interleaved_matrix)


%=========== inny sposob interleavingu
% interleaved_matrix = convintrlv(S,5,2) %inaczej przeplata



%% deinterlever (rozplot bitowy)

% R /R1 b�dzie wektorem otrzymanym po przepuszczeniu przez �wiat�ow�d

% ============= bez interleavingu
% R = code_with_noise 
% R = dlmread('R.dat');
% R=punctured_code;

% ============= przeplot o podanych wymiarach macierzy przeplotu
% R1 = matdeintrlv(code_with_noise,a,b);% wiadomosc odebrana z medium transmisyjnego po rozplocie
% R= R1(1:length_punctured_code); % usuniecie nadmiarowych bitow

% =============== przeplot o kwadratowych wymiarach macierzy l wierszy = l kolumn
% R1 = matdeintrlv(code_with_noise,interleaved_matrix_dimension,interleaved_matrix_dimension);
% R= R1(1:length_punctured_code)
% dlmwrite('wiadomosc_po_deinterleavingu.dat',R,'delimiter','\n');

