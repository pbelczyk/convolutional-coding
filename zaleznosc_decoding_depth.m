%% skrypt wykorzystywany w laboratorium do analizy warto�ci BER w zale�no�ci 
% od decoding depth wiadomo�ci przesy�aych przez medium transmisyjne jakim
% by� �wiat�ow�d wielomodowy, zawiera r�zne modyfikacje kwantyzera,
% mechanizm depuncturingu, deinterleavingu, algorytm Viterbiego
% zaimplementowany w wersji twardodecyzyjnej oraz mi�kkodecyzyjnej,
% algorytm ten ma zaimplementwany mechanizm okna przes�wnego

clear all 
close all
clc


%% parametry 

K =3; %d�ugo�� wymuszona (ilo�� kom�rek pami�ci kodera + ilos� bit�w wchodz�cych
% w danym takcie do kodera), K bit�w w danym takcie dzia�ania kodera wp�ywa
% na wyj�cie kodera

max_decoding_depth = 12*K; % max d�ugo�� okna przesuwnego (ang. sliding window),
% jest to g��boko�� dekodowania

sim = 1; % liczba blok�w wiadomosci


decoding_depth=3; % poc�tkowa warto�� g��boko�ci dekodowania, d�ugo�� okna 
%przesuwnego dekodera, optymalna warto�� 5*K, K - d�ugo�� wymuszona kodera

number_of_decoded_bits=1; %liczba bit�w przekazywana na wyj��ie dekodera w 
% ka�dym takcie jego dzia�ania; ilo�� bit�w po kt�rych okno przesuwne si� przesunie
%w procesie dekodowania

LENGTH_cv = 4096;  % d�ugo�� wiadomo�ci pojedynczego bloku (wej�ciowych bit�w, 
%nie zakodowanych, wchodz�cych do kodera)

memory_cv = 2;  % ilo�� kom�rek pami�ci kodera,
LENGTH_c_cv = 2*(memory_cv+LENGTH_cv); % d�ugo�� zakodowanej wiadomo�ci

CodeRate = 1/2; %code rate kodera
BER_HARD = []; %tablica przechowuj�ca BER, dekodowanie twardodecyzyjne
BER_SOFT = []; %tablica przechowuj�ca BER, dekodowanie mi�kkodecyzyjne

%% wielomany generuj�ce

 fprintf ('wielomiany generuj�ce:\n g0=[111]  g1=[101]\n');
 g_poly =[1 1 1 0 1 1]; %odpowied� impulsowa kodera
 %optymalne odczepy na podst. Convolutional Codes
 %with Optimum Distance Spectrum

%%
[id,kom] = fopen('ilosc bled�w od decoding depth.txt','wt'); %tworzenie pliku
%do zapisu wynik�w BER w zale�no�ci od metody dekodowania oraz g��boko�ci
%dekodowania
fprintf( id,'%s\n', 'decoding depth    hard            soft' );%nag��wki w pliku
    

% for snr=1:length(EbNodB) %p�tla, r�ne warto�ci Eb/No

% fprintf('\n ----------\n');
% fprintf('\nEbNo : %f\n',EbNodB(snr));

total_VB_Err_HARD = 0; %l b��d�w we wszystkich blokach hard dla danego Eb/No
total_VB_Err = 0; % l b��d�w we wszystkich blokach soft dla danego Eb/No


for block=1:sim % sim - ilo�� blok�w, r�nych wiadomo�ci generowanych dla danego Eb/No
    
%% binarna wiadomo�c wejsciowa kodera -wczytanie z pliku


m_cv = dlmread('wejscie_do_kodera.dat');% wczytanie binarnej wiadomo�ci
% wej�ciowej z pliku
% length(m_cv);


%% deinterlever (rozplot bitowy), w przypadku gdy wiadomo�� zakodowana
% zosta�a poddana przeplotowi, przed procesem dekodowania nale�y dokonac
% rozplotu
% R /R1 b�dzie wektorem otrzymanym po przepuszczeniu przez �wiat�ow�d

% ============= bez interleavingu
R = dlmread('R.dat'); %wczytanie pliku z oscyloskopu, zaiweraj�cego odebran�
% wiadomo�c binarn� po przes�aniu przez �wiat�ow�d, warto�ci s�
% nieunormowane, w procesie kwantyzacji nast�puje ich unormowanie


% ============= przeplot o podanych wymiarach macierzy przeplotu
% length_punctured_code = 8196; %d� wiadomo�ci zaakodowanej bez punkturingu
% % length_punctured_code = 5464; %d� wiadomo�ci zaakodowanej punc 3_4
% % length_punctured_code = 6147; % d� wiadomo�ci zaakodowanej punc 2_3
% a = 410; % l wierszy macierzy przeplotu
% b=20; % l kolumn macierzy przeplotu
% R1 = matdeintrlv(R,a,b);% wiadomosc odebrana z medium transmisyjnego po rozplocie
% R= R1(1:length_punctured_code); % usuniecie nadmiarowych bitow

% =============== przeplot o kwadratowych wymiarach macierzy l wierszy = l kolumn
% R1 = matdeintrlv(code_with_noise,interleaved_matrix_dimension,interleaved_matrix_dimension);
% R= R1(1:length_punctured_code)
% dlmwrite('wiadomosc_po_deinterleavingu.dat',R,'delimiter','\n');


%% unormowanie warto�ci �rodkowej pr�bek sygna�u z oscyloskopu w przypadku
% jakiej� niezerowej warto��i offset

max_wartosc = max(R); %sprawdzam max i min dla wybranych ju� pr�bek (co 10-tej z wycietej sekwencji)
min_wartosc = min(R);

% cbar = R>0; %jesli srodek wartosci sekwencji jest na 0 (nie przesuni�ty
% offset)

srodek = max_wartosc-(max_wartosc-min_wartosc)/2;
cbar = R>srodek; %jesli srodek wartosci sekwencji nie jest na 0 (offset przesuni�ty)

%fprintf('liczba b��d�w dla wiadomo�ci po detekcji znaku:\n');
%number_of_error = sum(abs(cbar - c_cv))


% cbar= dlmread('wejscie_dekodera_hard1.dat') %mo�liwo�� odczytu cbar z
% pliku utworzonego przez generator
dlmwrite('wejscie_dekodera_hard.dat',cbar,'delimiter','\n');

%% kwantyzacja pr�bek sygna�u otrzymanych z oscyloskopu, r�ne modyfikacje
% kwantyzer�w przebadane w oddzielnym skrypcie (wybor_kwant.m) w celu
% uzyskania najwi�kszego zysku kodowego stosowac kwantyzer wybrany przez ww
% skrypt


% % offset przesuniety (srodek wartosci przeslanej nie jest na zerze)


% %================================= rownomierna z przesunietym offsetem
% %(srodek wartosci przeslanej nie jest na zerze)
% center = srodek;
% part2 = ((max_wartosc-min_wartosc)/2)/4; %wartosc max probki/4
% partition = [center-3*part2, center-2*part2, center-part2, center, center+part2, center+2*part2, center+3*part2];
% codebook = [-1, -.75, -.5, -.25, .25, .5, .75, 1];

%============================ nierownomierna najlepsza z offset
% przesunietym
center = srodek;
max = (max_wartosc-min_wartosc)/2;
partition = [center-0.75*max, center-0.285*max, center-0.072*max, center, center+0.072*max, center+0.285*max, center+0.75*max];
codebook = [-1, -.7, -.25, -.05, .05, .25, .7, 1];

% % ============================ 1 nierownomierna gorsza z offset
% % przesunietym
% center = srodek;
% max = (max_wartosc-min_wartosc)/2;
% partition = [center-0.69*max, center-0.25*max, center-0.06*max, center, center+0.06*max, center+0.25*max, center+0.69*max];
% codebook = [-1, -.75, -.5, -.25, .25, .5, .75, 1];

% % ============================ 2 nierownomierna gorsza z offset
% % przesunietym
% center = srodek;
% max = (max_wartosc-min_wartosc)/2;
% partition = [center-0.69*max, center-0.25*max, center-0.06*max, center, center+0.06*max, center+0.25*max, center+0.69*max];
% codebook = [-1, -.7, -.25, -.05, .05, .25, .7, 1];


% % ============================ 3 nierownomierna gorsza z offset
% % przesunietym
% center = srodek;
% max = (max_wartosc-min_wartosc)/2;
% partition = [center-0.6*max, center-0.2*max, center-0.05*max, center, center+0.05*max, center+0.2*max, center+0.6*max];
% codebook = [-1, -.7, -.25, -.05, .05, .25, .7, 1];


% ============================ nierownomierna tez dobra z offset
% % przesunietym
% center = srodek;
% max = (max_wartosc-min_wartosc)/2;
% partition = [center-0.75*max, center-0.285*max, center-0.072*max, center, center+0.072*max, center+0.285*max, center+0.75*max];
% codebook = [-1, -.885, -.531, -.05, .05, .531, .885, 1];




%% offset na 0

% %============================== rownomierna z offset na 0
% %(srodek wartosci przeslanej jest na zerze)
% center = 0;
% part2 = max_wartosc/4; %wartosc max probki/4
% partition = [center-3*part2, center-2*part2, center-part2, center, center+part2, center+2*part2, center+3*part2];
% codebook = [-1, -.75, -.5, -.25, .25, .5, .75, 1];

% % ============================ nierownomierna najlepsza z offset na 0
% center = 0;
% max = max_wartosc;
% partition = [center-0.75*max, center-0.285*max, center-0.072*max, center, center+0.072*max, center+0.285*max, center+0.75*max];
% codebook = [-1, -.7, -.25, -.05, .05, .25, .7, 1];


% %============================ 1 nierownomierna gorsza z offset na 0
% center = 0;
% max = max_wartosc;
% partition = [center-0.69*max, center-0.25*max, center-0.06*max, center, center+0.06*max, center+0.25*max, center+0.69*max];
% codebook = [-1, -.75, -.5, -.25, .25, .5, .75, 1];

% %============================ 2 nierownomierna gorsza z offset na 0
% center = 0;
% max = max_wartosc;
% partition = [center-0.69*max, center-0.25*max, center-0.06*max, center, center+0.06*max, center+0.25*max, center+0.69*max];
% codebook = [-1, -.7, -.25, -.05, .05, .25, .7, 1];

% %============================ 3 nierownomierna gorsza z offset na 0
% center = 0;
% max = max_wartosc;
% partition = [center-0.6*max, center-0.2*max, center-0.05*max, center, center+0.05*max, center+0.2*max, center+0.6*max];
% codebook = [-1, -.7, -.25, -.05, .05, .25, .7, 1];

%% kwantyzacja przy u�yciu wybranego kwantyzera z powy�szych 

 [index, quants]=quantiz(R,partition,codebook);
 dlmwrite('quant.dat',quants,'delimiter',' ');
 quants;
% quants = dlmread('quant.dat');

%% depuncturing, w przypadku stosowania punkturingu (zmiany Code Rate z
% macierzystej na inn�) nalezy dokona� depunkturingu wiadomo�ci

%============ podstawowa sprawnosc 1/2
% nic nie trzeba wpisywac


%============ punctured matrix do sprawnosci 3/4

% depunctured_code_hard = ones(1,LENGTH_c_cv);
%  depunctured_code_hard = depunctured_code_hard *100;
%  depunctured_code_hard (1:3:end) = cbar(1:2:end);
%  depunctured_code_hard (2:3:end) = cbar(2:2:end);
%  cbar = depunctured_code_hard;
%  
%  depunctured_code_soft = ones(1,LENGTH_c_cv);
%  depunctured_code_soft = depunctured_code_soft *100;
%  depunctured_code_soft (1:3:end) = quants(1:2:end);
%  depunctured_code_soft (2:3:end) = quants(2:2:end);
%  quants = depunctured_code_soft;

%============ punctured matrix do sprawnosci 2/3
% 
 depunctured_code_hard = ones(1,LENGTH_c_cv);
 depunctured_code_hard = depunctured_code_hard *100;
 depunctured_code_hard (1:4:end) = cbar(1:3:end);
 depunctured_code_hard (2:4:end) = cbar(2:3:end);
 depunctured_code_hard (3:4:end) = cbar(3:3:end);
 cbar = depunctured_code_hard;

 depunctured_code_soft = ones(1,LENGTH_c_cv);
 depunctured_code_soft = depunctured_code_soft *100;
 depunctured_code_soft (1:4:end) = quants(1:3:end);
 depunctured_code_soft (2:4:end) = quants(2:3:end);
 depunctured_code_soft (3:4:end) = quants(3:3:end);
 quants = depunctured_code_soft;





 %============ punctured matrix do sprawnosci 5/6
  
% %10100
% %11011
%  depunctured_code_hard = ones(1,LENGTH_c_cv);
%  depunctured_code_hard = depunctured_code_hard *100;

%  depunctured_code_hard (1:10:end) = cbar(1:6:end);
%  depunctured_code_hard (2:10:end) = cbar(2:6:end);
%  depunctured_code_hard (4:10:end) = cbar(3:6:end);
%  depunctured_code_hard (5:10:end) = cbar(4:6:end);
%  depunctured_code_hard (8:10:end) = cbar(5:6:end);
%  depunctured_code_hard (10:10:end) = cbar(6:6:end);
%  cbar = depunctured_code_hard;
% 
%  depunctured_code_soft = ones(1,LENGTH_c_cv);
%  depunctured_code_soft = depunctured_code_soft *100;

%  depunctured_code_soft (1:10:end) = quants(1:6:end);
%  depunctured_code_soft (2:10:end) = quants(2:6:end);
%  depunctured_code_soft (4:10:end) = quants(3:6:end);
%  depunctured_code_soft (5:10:end) = quants(4:6:end);
%  depunctured_code_soft (8:10:end) = quants(5:6:end);
%  depunctured_code_soft (10:10:end) = quants(6:6:end);
%  quants = depunctured_code_soft;
 
 
 
%% stare punkturyzacje uzywane na pierwszym labo
%==================punctured matrix do sprawnosci 5/6 uzywane na pierwszym
%labo (folder: pierwsze laboratorium\przeslane przez swiatlow�d)

%  depunctured_code_hard = ones(1,LENGTH_c_cv);
%  depunctured_code_hard = depunctured_code_hard *100;
 
%  depunctured_code_hard (1:10:end) = cbar(1:6:end);
%  depunctured_code_hard (2:10:end) = cbar(2:6:end);
%  depunctured_code_hard (4:10:end) = cbar(3:6:end);
%  depunctured_code_hard (5:10:end) = cbar(4:6:end);
%  depunctured_code_hard (8:10:end) = cbar(5:6:end);
%  depunctured_code_hard (9:10:end) = cbar(6:6:end);
%  cbar = depunctured_code_hard;
% 
%  depunctured_code_soft = ones(1,LENGTH_c_cv);
%  depunctured_code_soft = depunctured_code_soft *100;
 
%  depunctured_code_soft (1:10:end) = quants(1:6:end);
%  depunctured_code_soft (2:10:end) = quants(2:6:end);
%  depunctured_code_soft (4:10:end) = quants(3:6:end);
%  depunctured_code_soft (5:10:end) = quants(4:6:end);
%  depunctured_code_soft (8:10:end) = quants(5:6:end);
%  depunctured_code_soft (9:10:end) = quants(6:6:end);
%  quants = depunctured_code_soft;
% 
%  
 
%==================punctured matrix do sprawnosci 2_3 uzywane na pierwszym
%labo stare gorsze

% depunctured_code_hard = ones(1,LENGTH_c_cv);
%  depunctured_code_hard = depunctured_code_hard *100;
%  depunctured_code_hard (1:4:end) = cbar(1:3:end);
%  depunctured_code_hard (2:4:end) = cbar(2:3:end);
%  depunctured_code_hard (4:4:end) = cbar(3:3:end);
%  cbar = depunctured_code_hard;
% 
%  depunctured_code_soft = ones(1,LENGTH_c_cv);
%  depunctured_code_soft = depunctured_code_soft *100;
%  depunctured_code_soft (1:4:end) = quants(1:3:end);
%  depunctured_code_soft (2:4:end) = quants(2:3:end);
%  depunctured_code_soft (4:4:end) = quants(3:3:end);
%  quants = depunctured_code_soft;
%  

dlmwrite('wejscie_dekodera_hard_po_depun.dat',cbar,'delimiter','\n');
% cbar = dlmread('wejscie_dekodera_hard_po_depun.dat');
% cbar1 = dlmread('wyjscie_kodera1.dat');
% cbar = cbar1'

%% algorytm Viterbiego twardodecyzyjny
N = LENGTH_cv+memory_cv+1;% N = Length_cv + memory_cv +1?
% For a 15-bit message with two encoder memory flushing bits, there are 17 time instants in addition 
%to t = 0, which represents the initial condition of the encoder
%N -1 to liczba krok�w kodowania wiadomo�ci od zerowego rejestru na pocz�tku
%do zerowego rejestru na koniec (zera w kom pamieci kodera)

m_av_hard = zeros (1,LENGTH_cv+memory_cv); % wiadomosc odkodowana, bedzie 
%taka ile krok�w kodowania czyli o memory_cv za du�a

NS = zeros (4,3); % tablica przej�c do nast�pnych stan�w "next state table", pierwsza kolumna to 
%current state, druga kolumna to next state if input = '0', trzecia if
%input ='1'

NS(1,1) = 1; % 1 <-- stan '00'
NS(1,2) = 1;
NS(2,2)=1;
NS(2,1)= 2; NS(3,2)=2; NS(4,2) = 2; % 2<-- stan '01'
NS(1,3)=3; NS(2,3)=3; NS(3,1) = 3; % 3 <-- stan '10'
NS(3,3)=4; NS(4,1)=4; NS(4,3) = 4;

%NS;

% OS tablica wyj�ciowych symboli kolejno wiersz 1 to stan 1, wiersz 2 stan 2,
% kolumna 1 i2 wejsciowy symbol '0', kolumna 3 i 4 wejsciowy symbol '1'
OS = [0 0 1 1; 1 1 0 0; 1 0 0 1; 0 1 1 0];% tablica wyjsciowych symboli

CS_NS = zeros(4,4);% tablica pokazuj�ca jaki bit spowodowa� przejscie z current state do next state
%wiersze to kolejne current state, kolumny kolejne next state, pola to bity
%input
CS_NS(1,1)=0; CS_NS(1,3)=1;
CS_NS(2,1)=0; CS_NS(2,3)=1;
CS_NS(3,2)=0; CS_NS(3,4)=1;
CS_NS(4,2)=0; CS_NS(4,4)=1;
CS_NS;


n=2; % z code rate ile bit�w generowanych w 1 takcie
    
% ------------------------------------------------------------------------------------------

 %% p�tla zwiekszaj�ca warto�� g��boko�ci dekodowania, w ka�dej iteracji
 % obliczany i zapisywany do pliku BER dla danej g��boko�ci dekodowania i
 % metody dekodowania
 
number_of_error_hard = []; %liczba b��d�w w zale�no�ci od decoding depth 
% dekodowania twardodecyzyjne
number_of_error_soft = []; %liczba b��d�w w zale�no�ci od decoding depth 
% dekodowania miekkodecyzyjne

for new_decoding_depth = decoding_depth:3: max_decoding_depth
   
 decoding_depth=new_decoding_depth; %zmiana wartosci decoding depth z kazd� iteracj�
    
 N1=number_of_decoded_bits+decoding_depth+1; %N1-1=number_of_decoded_bits+decoding_depth
 %N1-1 jest to ilosc taktow po ktorych zdekoduje czesc ciagu nadanego, N1-1 = N+D
 % gdzie N to ilosc bit�w kt�re bed� odkodowane (number_of_decoded_bits), D (decoding_depth) =
 % 3 do 5*K gdzie K to d�ugosc wymuszona, wszystkie �cie�ki przetrwania po zdekodowaniu odpowiednio duzego bloku danych
 %pokrywaja sie na pocz�tku
 
 m_av_hard = zeros(1,1);
 OS = [0 0 1 1; 1 1 0 0; 1 0 0 1; 0 1 1 0];% tablica wyjsciowych symboli
 
 SS = zeros (1,N); % state sequence, sekwencja stan�w przez jakie przechodzi� koder
 %koduj�c wiadomo�� wej�ciow�
AEM = zeros (4,N); %accumulated error metrics, tablica skumulowanych metryk b��d�w
SPH = zeros (4,N);% state predecessor history, historia poprzednich stan�w
 
 %% Viterbi twardodecyzyjny
 %% pierwsza faza dzia�ania alg Viterbiego, obliczenie metryk doj�cia do
 % wszystkich mo�liwych stan�w wykresu kratowego, algorytm zaczyna dzia�anie
 %w stanie '00', stany definiowane zawarto�ci� kom�rek pami�ci algorytm Viterbiego r�zni
 % sie od stosowanego w fazie regularnej; w pierwszej fazie �cie�ki na
 % wykresie kratowym musz� 'rozrosn��' sie do kazdego mo�liwego stanu
 
 i4=1; %licznik takt�w dekodera

% sprawdzanie metryk dochodz�cych do stanu 00
d1 = 0;
    if cbar(2*i4-1) ~= 100%sprawdza czy nie jest to symbol dodany przez depuncturing
        %puncturing nie u�ywany w teoretycznych wykresach, tylko w
        %laboratorium
        d1 = OS(1,1) ~= cbar(2*i4-1); %obliczanie odleg�osci Hamminga b�dacej metryka
        %cz�ciow� ga��zi
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d1 = d1 + (OS(1,2) ~= cbar(2*i4-1+n-1));%obliczanie odleg�osci Hamminga b�dacej 
        % ca�o�ciow� metryk� ga��zi
    end
 % sprawdzanie metryk dochodz�cych do stanu 10   
 d5=0;
 
    if cbar(2*i4-1) ~= 100
        d5 = OS(1,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d5 = d5 + (OS(1,4) ~= cbar(2*i4-1+n-1));
    end
%=================
%             if AEM(1,i4)+d1<AEM(1,i4)+d5
                SPH(1,i4)=1; %zapis do tablicy survivor path history z kt�rego stanu
                %przeszed� algorytm w danym takcie do danego stanu; stan 1
                %-> '00' 2 -> '01' 3 -> '10' 4 -> '11'
                AEM(1,i4+1)=AEM(1,i4)+d1; %zapis do tablicy accumulated error metric
                %skumulowanej metryki przejscia zwi�zanej z dan� ga��zi�
%             else
                SPH(3,i4)=1;
                AEM(3,i4+1)=AEM(1,i4)+d5;
%             end
            
i4=2;

% sprawdzanie metryk dochodz�cych do stanu 00
d1 = 0;
    if cbar(2*i4-1) ~= 100
        d1 = OS(1,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d1 = d1 + (OS(1,2) ~= cbar(2*i4-1+n-1));
    end
% sprawdzanie metryk dochodz�cych do stanu 10    
 d5=0;
 
    if cbar(2*i4-1) ~= 100
        d5 = OS(1,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d5 = d5 + (OS(1,4) ~= cbar(2*i4-1+n-1));
    end
%================

%             if AEM(1,i4)+d1<AEM(1,i4)+d5
                SPH(1,i4)=1;
                AEM(1,i4+1)=AEM(1,i4)+d1;
%             else
                SPH(3,i4)=1;
                AEM(3,i4+1)=AEM(1,i4)+d5;
%             end  

% sprawdzanie metryk dochodz�cych do stanu 01
d3=0;
    if cbar(2*i4-1) ~= 100
        d3 = OS(3,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d3 = d3 + (OS(3,2) ~= cbar(2*i4-1+n-1));
    end
% sprawdzanie metryk dochodz�cych do stanu 11
d7=0;
    if cbar(2*i4-1) ~= 100
        d7 = OS(3,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d7 = d7 + (OS(3,4) ~= cbar(2*i4-1+n-1));
    end
%================
    
%                if AEM(3,i4)+d3<AEM(3,i4)+d7
                 SPH(2,i4)=3;
                 AEM(2,i4+1)=AEM(3,i4)+d3;
%                else
                 SPH(4,i4)=3;
                 AEM(4,i4+1)=AEM(3,i4)+d7;
%                end
               %AEM(1,i4+1)
 
%% faza regularna alg Viterbiego, p�tla do wyliczania metryk doj�cia do
% wszystkich stan�w, w tej fazie z kazdego stanu mozna doj�c do dw�ch
% mozliwych stan�w w takcie nastepnym
%tablice AEM, SPH uzupe�niane przez (number_of_decoded_bits+5*K)takt�w
 % d�ugo�� = sliding windows + number_of_decoded_bits
 
 for i4=3:N1-1%licznik do wpisywania w kolumny tablic

% sprawdzanie metryk dochodz�cych do stanu 00
d1 = 0;% r�nice mi�dzy zakodowanym 11 a sekwencja wyjsciowa kodera
%              % dla przej�cia do stanu 00 ze stanu 00
    if cbar(2*i4-1) ~= 100
        d1 = OS(1,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d1 = d1 + (OS(1,2) ~= cbar(2*i4-1+n-1));
    end
  
d2 = 0;% r�nice mi�dzy zakodowanym 11 a sekwencja wyjsciowa kodera
%              % dla przej�cia do stanu 00 ze stanu 01
    if cbar(2*i4-1) ~= 100
        d2 = OS(2,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d2 = d2 + (OS(2,2) ~= cbar(2*i4-1+n-1));
    end
%===================     

             if AEM(1,i4)+d1<AEM(2,i4)+d2
                  SPH(1,i4)=1;%przeszli�my do stanu '00' ze stanu '00'
                  AEM(1,i4+1)=AEM(1,i4)+d1;
             else
                 SPH(1,i4)=2;% przeszlismy do stanu 00 ze stanu 01
                 AEM(1,i4+1)=AEM(2,i4)+d2;
             end
             
% sprawdzanie metryk dochodz�cych do stanu 01
d3=0;
    if cbar(2*i4-1) ~= 100
        d3 = OS(3,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d3 = d3 + (OS(3,2) ~= cbar(2*i4-1+n-1));
    end
    
d4=0;
    if cbar(2*i4-1) ~= 100
        d4 = OS(4,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d4 = d4 + (OS(4,2) ~= cbar(2*i4-1+n-1));
    end
%==================
               if AEM(3,i4)+d3<AEM(4,i4)+d4
                 SPH(2,i4)=3;
                 AEM(2,i4+1)=AEM(3,i4)+d3;
               else
                 SPH(2,i4)=4;
                 AEM(2,i4+1)=AEM(4,i4)+d4;
               end
               
% sprawdzanie metryk dochodz�cych do stanu 10
d5=0;
    if cbar(2*i4-1) ~= 100
        d5 = OS(1,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d5 = d5 + (OS(1,4) ~= cbar(2*i4-1+n-1));
    end
    
d6=0;
    if cbar(2*i4-1) ~= 100
        d6 = OS(2,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d6 = d6 + (OS(2,4) ~= cbar(2*i4-1+n-1));
    end
%===================
             if AEM(1,i4)+d5<AEM(2,i4)+d6
               SPH(3,i4)=1;
               AEM(3,i4+1)=AEM(1,i4)+d5;
             else
               SPH(3,i4)=2;
               AEM(3,i4+1)=AEM(2,i4)+d6;
             end

% sprawdzanie metryk dochodz�cych do stanu 11
d7=0;
    if cbar(2*i4-1) ~= 100
        d7 = OS(3,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d7 = d7 + (OS(3,4) ~= cbar(2*i4-1+n-1));
    end
    
d8=0;
    if cbar(2*i4-1) ~= 100
        d8 = OS(4,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d8 = d8 + (OS(4,4) ~= cbar(2*i4-1+n-1));
    end
%=================
              if AEM(3,i4)+d7<AEM(4,i4)+d8
                  SPH(4,i4)=3;
                  AEM(4,i4+1)=AEM(3,i4)+d7;
              else
                  SPH(4,i4)=4;
                  AEM(4,i4+1)=AEM(4,i4)+d8;
              end
  
 end
 
AEM(:,N1);
[r,c]=find(AEM(:,N1)==min(min(AEM(:,N1))));  % r wskazuje wiersz z min odl hamminga
%na jego podstawie okre�lane jest SPH, najmniejsza metryka wskazuje �cie�ke
%na wykresie kratowym

%fprintf('nr stanu z najmniejsza metryka');
% r1=r(1,1);

SS(N1)=r1;

% generowanie tablicy stan�w przez jakie przechodzi� algorytm
clearvars i1;

for i1 = (N1-1): -1: 1
SS(i1)=SPH(SS(i1+1),i1); 
end


%obliczanie bit�w jakie wesz�y do kodera na podstawie tablicy przej�c
%pomiedzy stanem current a next
for i1=1:number_of_decoded_bits
   m_av_hard(i1) = CS_NS(SS(i1), SS(i1+1));
    SS(i1);
SS(i1+1);
    
end
%fprintf('\n pierwsza czesc zdekodowanej wiadomosci');
m_av_hard();

%%            koniec dzialania petli do pierwszego dekodowania
%do tego momentu zdekoduje wiadomosc o il bit�w = number_of_decoded_bits;
% od tego momentu okno przesuwne bedzie si� przesuwac po trellisie i na
% wyjsciu dekodera zwraca� number_of_decoded_bits bit�w zdekodowanych
% ------------------------------------------------------------------------------------


SPH = circshift(SPH, [0, -number_of_decoded_bits]);%przesuwamy o liczbe odkodowanych bit�w, stan na kt�rym sie
%zatrzymalismy przy dekodowaniu nadal zostanie w tablicach i z niego
%ruszymy w nast�pnym kroku trellisa
AEM = circshift(AEM, [0, -number_of_decoded_bits]);
SS = circshift(SS, [0, -number_of_decoded_bits]);


k=0;

krok = 1;
while N1-1+number_of_decoded_bits+k<=N-1 %petla po ca�ej d�ugo�ci zakodowanej wiadomosci
last_aem_column=N1-number_of_decoded_bits; % musi sie wyrownywac po kazdej petli for bo co 3 takty
%ktore obejmuje kazda petla for znow przysowamy macierz
last_sph_column=N1-number_of_decoded_bits-1; % musi sie wyrownywac po kazdej petli for bo co 3 takty
%ktore obejmuje kazda petla for znow przysowamy macierz


 for i4=N1+k:N1-1+number_of_decoded_bits+k%licznik do wpisywania w kolumny tablic

% sprawdzanie metryk dochodz�cych do stanu 00
d1 = 0;% r�nice mi�dzy zakodowanym 11 a sekwencja wyjsciowa kodera
%              % dla przej�cia do stanu 00 ze stanu 00
    if cbar(2*i4-1) ~= 100
        d1 = OS(1,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d1 = d1 + (OS(1,2) ~= cbar(2*i4-1+n-1));
    end
  
d2 = 0;% r�nice mi�dzy zakodowanym 11 a sekwencja wyjsciowa kodera
%              % dla przej�cia do stanu 00 ze stanu 01
    if cbar(2*i4-1) ~= 100
        d2 = OS(2,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d2 = d2 + (OS(2,2) ~= cbar(2*i4-1+n-1));
    end
%===================     

             if AEM(1,last_aem_column)+d1<AEM(2,last_aem_column)+d2
                  SPH(1,last_aem_column)=1;%przeszli�my do stanu '00' ze stanu '00'
                  AEM(1,last_aem_column+1)=AEM(1,last_aem_column)+d1;
             else
                 SPH(1,last_aem_column)=2;% przeszlismy do stanu 00 ze stanu 01
                 AEM(1,last_aem_column+1)=AEM(2,last_aem_column)+d2;
             end

% sprawdzanie metryk dochodz�cych do stanu 01
d3=0;
    if cbar(2*i4-1) ~= 100
        d3 = OS(3,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d3 = d3 + (OS(3,2) ~= cbar(2*i4-1+n-1));
    end
    
d4=0;
    if cbar(2*i4-1) ~= 100
        d4 = OS(4,1) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d4 = d4 + (OS(4,2) ~= cbar(2*i4-1+n-1));
    end
%==================
               if AEM(3,last_aem_column)+d3<AEM(4,last_aem_column)+d4
                 SPH(2,last_aem_column)=3;
                 AEM(2,last_aem_column+1)=AEM(3,last_aem_column)+d3;
               else
                 SPH(2,last_aem_column)=4;
                 AEM(2,last_aem_column+1)=AEM(4,last_aem_column)+d4;
               end

% sprawdzanie metryk dochodz�cych do stanu 10
d5=0;
    if cbar(2*i4-1) ~= 100
        d5 = OS(1,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d5 = d5 + (OS(1,4) ~= cbar(2*i4-1+n-1));
    end
    
d6=0;
    if cbar(2*i4-1) ~= 100
        d6 = OS(2,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d6 = d6 + (OS(2,4) ~= cbar(2*i4-1+n-1));
    end
%===================
             if AEM(1,last_aem_column)+d5<AEM(2,last_aem_column)+d6
               SPH(3,last_aem_column)=1;
               AEM(3,last_aem_column+1)=AEM(1,last_aem_column)+d5;
             else
               SPH(3,last_aem_column)=2;
               AEM(3,last_aem_column+1)=AEM(2,last_aem_column)+d6;
             end

% sprawdzanie metryk dochodz�cych do stanu 11
d7=0;
    if cbar(2*i4-1) ~= 100
        d7 = OS(3,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d7 = d7 + (OS(3,4) ~= cbar(2*i4-1+n-1));
    end
    
d8=0;
    if cbar(2*i4-1) ~= 100
        d8 = OS(4,3) ~= cbar(2*i4-1);
    end
    if cbar(2*i4-1+n-1) ~= 100 
        d8 = d8 + (OS(4,4) ~= cbar(2*i4-1+n-1));
    end
%=================
              if AEM(3,last_aem_column)+d7<AEM(4,last_aem_column)+d8
                  SPH(4,last_aem_column)=3;
                  AEM(4,last_aem_column+1)=AEM(3,last_aem_column)+d7;
              else
                  SPH(4,last_aem_column)=4;
                  AEM(4,last_aem_column+1)=AEM(4,last_aem_column)+d8;
              end
  
   last_aem_column=last_aem_column+1;
   last_sph_column=last_sph_column+1;
   
 end  
 
    %fprintf ('\nSurvivor path metrics are:\n'); 
   % fprintf('\n\n\nSPH z kroku petli numer');
    krok;
SPH;
%fprintf ('\nAccumulated error metrics are:\n');
AEM;
 
clearvars SS
% fprintf('alst_aem za forem');
last_aem_column;
AEM(:,last_aem_column);
[r,c]=find(AEM(:,last_aem_column)==min(min(AEM(:,last_aem_column)))); % r wskazuje wiersz z min odl hamminga
r1=r(1,1);

SS(last_aem_column)=r1;
% generowanie tablicy stan�w przez jakie przechodzi� algorytm
for i1=last_aem_column -1: -1: 1 %SS obliczane od konca trellis tego do samego pocz�tku
i1;
SS(i1)=SPH(SS(i1+1),i1);

    
    
end
SS;

%obliczanie bit�w jakie wesz�y do kodera na podstawie tablicy przej�c
%pomiedzy stanem current a next
for i1=number_of_decoded_bits+1+k:2*number_of_decoded_bits+k
   m_av_hard(i1) = CS_NS(SS(i1-k-number_of_decoded_bits), SS(i1-k-number_of_decoded_bits+1));
   SS(i1-k-number_of_decoded_bits);
SS(i1-k-number_of_decoded_bits+1);
    
end
%fprintf('zdekodowana wiadomosc w kroku petli while');
% m_av_hard();

%przesuwanie tablic
AEM = circshift(AEM, [0, -number_of_decoded_bits]);
SPH = circshift(SPH, [0, -number_of_decoded_bits]);
SS = circshift(SS, [0, -number_of_decoded_bits]);


k=k+number_of_decoded_bits;
krok=krok+1;

end
%fprintf('\n \n po wyjsciu z while');


%fprintf('do odkodowania po while o dlugosci decoding depth');
% licznik1=number_of_decoded_bits+1+k;
% licznik2=number_of_decoded_bits+1+k+decoding_depth-1;
for i1=number_of_decoded_bits+1+k:number_of_decoded_bits+1+k+decoding_depth-1
   m_av_hard(i1) = CS_NS(SS(i1-k-number_of_decoded_bits), SS(i1-k-number_of_decoded_bits+1));
end
m_av_hard ;
length(m_av_hard);
%%

%wiadomo�� odkodowana bez pustych znak�w z konca, (s� zakodowane same zera
%przez koder podczas wypr�niania pami�ci kodera)
m_av_hard1 = m_av_hard(1:LENGTH_cv);%do length_cv ignorowanie pustych znak�w
dlmwrite('wyjscie_dekodera.dat',m_av_hard1,'delimiter',' ');

VB_Err_HARD = sum(abs(m_av_hard1-m_cv)) % liczba b��d�w w pojedynczym bloku

%fprintf('\n liczba b��d�w we wszystkich blokach hard viterbi');
total_VB_Err_HARD = total_VB_Err_HARD + VB_Err_HARD; % liczba wszystkich b��d�w wystpuj�cych w ilosci blokow
% = sim, dla konkretnej wartosci EbNo


number_of_error_hard = [number_of_error_hard VB_Err_HARD];


%% Viterbi soft decision


m_av_soft = zeros (1,1);%LENGTH_cv+memory_cv); % wiadomosc odkodowana, bedzie taka ile krok�w kodowania czyli o 
%memory_cv za du�a


% OS tablica wyj�ciowych symboli kolejno wiersz 1 to stan 1, wiersz 2 stan 2,
% kolumna 1 i2 wejsciowy symbol '0', kolumna 3 i 4 wejsciowy symbol '1'
OS = [-1 -1 1 1; 1 1 -1 -1; 1 -1 -1 1; -1 1 1 -1];%tablica wyjsciowych symboli warto�ci -1 zamiast 0 w stosunku
%do hard viterbi bo wektor do por�wnania z OS to R kt�ry jest bpsk + szum



 SS = zeros (1,N); % state sequence
AEM = zeros (4,N); %accumylated error metrics
SPH = zeros (4,N);% state predecessor history, historia poprzednich stan�w


 %% pierwsza faza dzia�ania alg Viterbiego, obliczenie metryk doj�cia do
 % wszystkich mo�liwych stan�w wykresu kratowego, algorytm zaczyna dzia�anie
 %w stanie '00', stany definiowane zawarto�ci� kom�rek pami�ci algorytm Viterbiego r�zni
 % sie od stosowanego w fazie regularnej; w pierwszej fazie �cie�ki na
 % wykresie kratowym musz� 'rozrosn��' sie do kazdego mo�liwego stanu
 
 i4=1;
% sprawdzanie metryk dochodz�cych do stanu 00
d1 = 0;
    if quants(2*i4-1) ~= 100
        d1 = (abs( quants(2*i4-1) - OS(1,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d1 = d1 + (abs( quants(2*i4-1+n-1) - OS(1,2) ))^2;
    end
 % sprawdzanie metryk dochodz�cych do stanu 10   
 d5=0;
 
    if quants(2*i4-1) ~= 100
        d5 = (abs( quants(2*i4-1) - OS(1,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d5 = d5 + (abs( quants(2*i4-1+n-1) - OS(1,4) ))^2;
    end
%=================
%             if AEM(1,i4)+d1<AEM(1,i4)+d5
                SPH(1,i4)=1;
                AEM(1,i4+1)=AEM(1,i4)+d1;
%             else
                SPH(3,i4)=1;
                AEM(3,i4+1)=AEM(1,i4)+d5;
%             end
      
i4=2;

% sprawdzanie metryk dochodz�cych do stanu 00
d1 = 0;
    if quants(2*i4-1) ~= 100
        d1 = (abs( quants(2*i4-1) - OS(1,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d1 = d1 + (abs( quants(2*i4-1+n-1) - OS(1,2) ))^2;
    end
 % sprawdzanie metryk dochodz�cych do stanu 10   
 d5=0;
 
    if quants(2*i4-1) ~= 100
        d5 = (abs( quants(2*i4-1) - OS(1,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d5 = d5 + (abs( quants(2*i4-1+n-1) - OS(1,4) ))^2;
    end
%=================
        
%             if AEM(1,i4)+d1<AEM(1,i4)+d5
                SPH(1,i4)=1;
                AEM(1,i4+1)=AEM(1,i4)+d1;
%             else
                SPH(3,i4)=1;
                AEM(3,i4+1)=AEM(1,i4)+d5;
%             end
            

% sprawdzanie metryk dochodz�cych do stanu 00
d3 = 0;
    if quants(2*i4-1) ~= 100
        d3 = (abs( quants(2*i4-1) - OS(3,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d3 = d3 + (abs( quants(2*i4-1+n-1) - OS(3,2) ))^2;
    end
 % sprawdzanie metryk dochodz�cych do stanu 10   
 d7=0;
 
    if quants(2*i4-1) ~= 100
        d7 = (abs( quants(2*i4-1) - OS(3,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d7 = d7 + (abs( quants(2*i4-1+n-1) - OS(3,4) ))^2;
    end
%=================
%                if AEM(3,i4)+d3<AEM(3,i4)+d7
                 SPH(2,i4)=3;
                 AEM(2,i4+1)=AEM(3,i4)+d3;
%                else
                 SPH(4,i4)=3;
                 AEM(4,i4+1)=AEM(3,i4)+d7;
%                end
               %AEM(1,i4+1)
   
%% faza regularna alg Viterbiego, p�tla do wyliczania metryk doj�cia do
% wszystkich stan�w, w tej fazie z kazdego stanu mozna doj�c do dw�ch
% mozliwych stan�w w takcie nastepnym
%tablice AEM, SPH uzupe�niane przez (number_of_decoded_bi
 
 for i4=3:N1-1%licznik do wpisywania w kolumny tablic
     i4;

% sprawdzanie metryk dochodz�cych do stanu 00
d1 = 0;% r�nice mi�dzy zakodowanym 11 a sekwencja wyjsciowa kodera
%              % dla przej�cia do stanu 00 ze stanu 00
    if quants(2*i4-1) ~= 100
        d1 = (abs( quants(2*i4-1) - OS(1,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d1 = d1 + (abs( quants(2*i4-1+n-1) - OS(1,2) ))^2;
    end
   
 d2=0;% r�nice mi�dzy zakodowanym 11 a sekwencja wyjsciowa kodera
%              % dla przej�cia do stanu 00 ze stanu 01
 
    if quants(2*i4-1) ~= 100
        d2 = (abs( quants(2*i4-1) - OS(2,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d2 = d2 + (abs( quants(2*i4-1+n-1) - OS(2,2) ))^2;
    end
%=================
             if AEM(1,i4)+d1<AEM(2,i4)+d2
                  SPH(1,i4)=1;%przeszli�my do stanu '00' ze stanu '00'
                  AEM(1,i4+1)=AEM(1,i4)+d1;
             else
                 SPH(1,i4)=2;% przeszlismy do stanu 00 ze stanu 01
                 AEM(1,i4+1)=AEM(2,i4)+d2;
             end
             
% sprawdzanie metryk dochodz�cych do stanu 01
d3 = 0;

    if quants(2*i4-1) ~= 100
        d3 = (abs( quants(2*i4-1) - OS(3,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d3 = d3 + (abs( quants(2*i4-1+n-1) - OS(3,2) ))^2;
    end
   
d4=0;
 
    if quants(2*i4-1) ~= 100
        d4 = (abs( quants(2*i4-1) - OS(4,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d4 = d4 + (abs( quants(2*i4-1+n-1) - OS(4,2) ))^2;
    end
%=================
               if AEM(3,i4)+d3<AEM(4,i4)+d4
                 SPH(2,i4)=3;
                 AEM(2,i4+1)=AEM(3,i4)+d3;
               else
                 SPH(2,i4)=4;
                 AEM(2,i4+1)=AEM(4,i4)+d4;
               end
               
% sprawdzanie metryk dochodz�cych do stanu 10 
d5 = 0;

    if quants(2*i4-1) ~= 100
        d5 = (abs( quants(2*i4-1) - OS(1,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d5 = d5 + (abs( quants(2*i4-1+n-1) - OS(1,4) ))^2;
    end
   
d6=0;
 
    if quants(2*i4-1) ~= 100
        d6 = (abs( quants(2*i4-1) - OS(2,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d6 = d6 + (abs( quants(2*i4-1+n-1) - OS(2,4) ))^2;
    end
%=================
             if AEM(1,i4)+d5<AEM(2,i4)+d6
               SPH(3,i4)=1;
               AEM(3,i4+1)=AEM(1,i4)+d5;
             else
               SPH(3,i4)=2;
               AEM(3,i4+1)=AEM(2,i4)+d6;
             end

% sprawdzanie metryk dochodz�cych do stanu 11
d7 = 0;

    if quants(2*i4-1) ~= 100
        d7 = (abs( quants(2*i4-1) - OS(3,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d7 = d7 + (abs( quants(2*i4-1+n-1) - OS(3,4) ))^2;
    end
   
d8=0;
 
    if quants(2*i4-1) ~= 100
        d8 = (abs( quants(2*i4-1) - OS(4,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d8 = d8 + (abs( quants(2*i4-1+n-1) - OS(4,4) ))^2;
    end
%=================
              if AEM(3,i4)+d7<AEM(4,i4)+d8
                  SPH(4,i4)=3;
                  AEM(4,i4+1)=AEM(3,i4)+d7;
              else
                  SPH(4,i4)=4;
                  AEM(4,i4+1)=AEM(4,i4)+d8;
              end
  
end 

AEM(:,N1);
[r,c]=find(AEM(:,N1)==min(min(AEM(:,N1)))); % r wskazuje wiersz z min odl hamminga
%fprintf('nr stanu z najmniejsza metryka');

r1=r(1,1);

SS(N1)=r1;
% generowanie tablicy stan�w przez jakie przechodzi� algorytm
clearvars i1;

for i1 = (N1-1): -1: 1
SS(i1)=SPH(SS(i1+1),i1); 
end

SS;
SPH;
AEM;
CS_NS;

%obliczanie bit�w jakie wesz�y do kodera na podstawie tablicy przej�c
%pomiedzy stanem current a next
for i1=1:number_of_decoded_bits
   m_av_soft(i1) = CS_NS(SS(i1), SS(i1+1));
    SS(i1);
SS(i1+1);
    
end
%fprintf('\n pierwsza czesc zdekodowanej wiadomosci');
m_av_soft();


%%            koniec dzialania petli do pierwszego dekodowania
%do tego momentu zdekoduje wiadomosc o il bit�w = number_of_decoded_bits;
% od tego momentu okno przesuwne bedzie si� przesuwac po trellisie i na
% wyjsciu dekodera zwraca� number_of_decoded_bits bit�w zdekodowanych
% ------------------------------------------------------------------------------------


%fprintf('\n po przesunieciu tablic');

SPH = circshift(SPH, [0, -number_of_decoded_bits]);%przesuwamy o liczbe odkodowanych bit�w, stan na kt�rym sie
%zatrzymalismy przy dekodowaniu nadal zostanie w tablicach i z niego
%ruszymy w nast�pnym kroku trellisa
AEM = circshift(AEM, [0, -number_of_decoded_bits]);
SS = circshift(SS, [0, -number_of_decoded_bits]);

% N1-1;
k=0;

krok = 1;

while N1-1+number_of_decoded_bits+k<=N-1 %tyle krok�w kodowania jest dla wiadomosci 17 bitowej
  
last_aem_column=N1-number_of_decoded_bits; %=5 musi sie wyrownywac po kazdej petli for bo co 3 takty
%ktore obejmuje kazda petla for znow przysowamy macierz
last_sph_column=N1-number_of_decoded_bits-1; %=4 musi sie wyrownywac po kazdej petli for bo co 3 takty
%ktore obejmuje kazda petla for znow przysowamy macierz


 for i4=N1+k:N1-1+number_of_decoded_bits+k%licznik do wpisywania w kolumny tablic

% sprawdzanie metryk dochodz�cych do stanu 00
d1 = 0;% r�nice mi�dzy zakodowanym 11 a sekwencja wyjsciowa kodera
%              % dla przej�cia do stanu 00 ze stanu 00
    if quants(2*i4-1) ~= 100
        d1 = (abs( quants(2*i4-1) - OS(1,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d1 = d1 + (abs( quants(2*i4-1+n-1) - OS(1,2) ))^2;
    end
   
 d2=0;% r�nice mi�dzy zakodowanym 11 a sekwencja wyjsciowa kodera
%              % dla przej�cia do stanu 00 ze stanu 01
 
    if quants(2*i4-1) ~= 100
        d2 = (abs( quants(2*i4-1) - OS(2,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d2 = d2 + (abs( quants(2*i4-1+n-1) - OS(2,2) ))^2;
    end
%=================
             if AEM(1,last_aem_column)+d1<AEM(2,last_aem_column)+d2
                  SPH(1,last_aem_column)=1;%przeszli�my do stanu '00' ze stanu '00'
                  AEM(1,last_aem_column+1)=AEM(1,last_aem_column)+d1;
             else
                 SPH(1,last_aem_column)=2;% przeszlismy do stanu 00 ze stanu 01
                 AEM(1,last_aem_column+1)=AEM(2,last_aem_column)+d2;
             end

% sprawdzanie metryk dochodz�cych do stanu 01
d3 = 0;

    if quants(2*i4-1) ~= 100
        d3 = (abs( quants(2*i4-1) - OS(3,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d3 = d3 + (abs( quants(2*i4-1+n-1) - OS(3,2) ))^2;
    end
   
d4=0;
 
    if quants(2*i4-1) ~= 100
        d4 = (abs( quants(2*i4-1) - OS(4,1) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d4 = d4 + (abs( quants(2*i4-1+n-1) - OS(4,2) ))^2;
    end
%=================
               if AEM(3,last_aem_column)+d3<AEM(4,last_aem_column)+d4
                 SPH(2,last_aem_column)=3;
                 AEM(2,last_aem_column+1)=AEM(3,last_aem_column)+d3;
               else
                 SPH(2,last_aem_column)=4;
                 AEM(2,last_aem_column+1)=AEM(4,last_aem_column)+d4;
               end

% sprawdzanie metryk dochodz�cych do stanu 10 
d5 = 0;

    if quants(2*i4-1) ~= 100
        d5 = (abs( quants(2*i4-1) - OS(1,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d5 = d5 + (abs( quants(2*i4-1+n-1) - OS(1,4) ))^2;
    end
   
d6=0;
 
    if quants(2*i4-1) ~= 100
        d6 = (abs( quants(2*i4-1) - OS(2,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d6 = d6 + (abs( quants(2*i4-1+n-1) - OS(2,4) ))^2;
    end
%=================
             if AEM(1,last_aem_column)+d5<AEM(2,last_aem_column)+d6
               SPH(3,last_aem_column)=1;
               AEM(3,last_aem_column+1)=AEM(1,last_aem_column)+d5;
             else
               SPH(3,last_aem_column)=2;
               AEM(3,last_aem_column+1)=AEM(2,last_aem_column)+d6;
             end

% sprawdzanie metryk dochodz�cych do stanu 11
d7 = 0;

    if quants(2*i4-1) ~= 100
        d7 = (abs( quants(2*i4-1) - OS(3,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d7 = d7 + (abs( quants(2*i4-1+n-1) - OS(3,4) ))^2;
    end
   
d8=0;
 
    if quants(2*i4-1) ~= 100
        d8 = (abs( quants(2*i4-1) - OS(4,3) ))^2;
    end
    if quants(2*i4-1+n-1) ~= 100 
        d8 = d8 + (abs( quants(2*i4-1+n-1) - OS(4,4) ))^2;
    end
%=================
              if AEM(3,last_aem_column)+d7<AEM(4,last_aem_column)+d8
                  SPH(4,last_aem_column)=3;
                  AEM(4,last_aem_column+1)=AEM(3,last_aem_column)+d7;
              else
                  SPH(4,last_aem_column)=4;
                  AEM(4,last_aem_column+1)=AEM(4,last_aem_column)+d8;
              end
  
   last_aem_column=last_aem_column+1;
   last_sph_column=last_sph_column+1;
   

 end  
 
 
    krok;
SPH;
%fprintf ('\nAccumulated error metrics are:\n');
AEM;

% fprintf('alst_aem za forem');
last_aem_column;
AEM(:,last_aem_column);
[r,c]=find(AEM(:,last_aem_column)==min(min(AEM(:,last_aem_column)))); % r wskazuje wiersz z min odl hamminga
r1=r(1,1);

SS(last_aem_column)=r1;
% generowanie tablicy stan�w przez jakie przechodzi� algorytm
for i1=last_aem_column -1: -1: 1 %SS obliczane od konca trellis tego do samego pocz�tku
i1;
SS(i1)=SPH(SS(i1+1),i1);
end


%obliczanie bit�w jakie wesz�y do kodera na podstawie tablicy przej�c
%pomiedzy stanem current a next
 for i1=number_of_decoded_bits+1+k:2*number_of_decoded_bits+k
   m_av_soft(i1) = CS_NS(SS(i1-k-number_of_decoded_bits), SS(i1-k-number_of_decoded_bits+1));
%    fprintf('pierwsza') ;
   SS(i1-k-number_of_decoded_bits);
SS(i1-k-number_of_decoded_bits+1); 
end
%fprintf('zdekodowana wiadomosc w kroku petli while');
 
m_av_soft();

%przesuwanie tablic
AEM = circshift(AEM, [0, -number_of_decoded_bits]);
SPH = circshift(SPH, [0, -number_of_decoded_bits]);
SS = circshift(SS, [0, -number_of_decoded_bits]);


k=k+number_of_decoded_bits;
krok=krok+1;

end
%fprintf('\n \n po wyjsciu z while');


%fprintf('do odkodowania po while o dlugosci decoding depth');
% licznik=number_of_decoded_bits+1+k;
% licznik2=number_of_decoded_bits+1+k+decoding_depth-1;
k;
for i1=number_of_decoded_bits+1+k:number_of_decoded_bits+1+k+decoding_depth-1
   m_av_soft(i1) = CS_NS(SS(i1-k-number_of_decoded_bits), SS(i1-k-number_of_decoded_bits+1));
end
m_av_soft;
length(m_av_soft);
%%


m_av_soft1 = m_av_soft(1:LENGTH_cv);%do length_cv ignorowanie pustych znak�w
dlmwrite('wyjscie_dekodera_soft.dat',m_av_soft1,'delimiter',' ');


VB_Err = sum(abs(m_av_soft1-m_cv)) % l b��d�w w pojedynczym bloku


total_VB_Err = total_VB_Err + VB_Err; % liczba wszystkich b��d�w wystpuj�cych w ilosci blokow
% = sim, dla konkretnej wartosci EbNo

number_of_error_soft = [number_of_error_soft VB_Err];

fprintf(id,'%7d', decoding_depth);
fprintf(id,'%15d %15d\n', VB_Err_HARD,VB_Err);
end
end
% calculate bit error rate
% BER_HARD = [BER_HARD total_VB_Err_HARD/(LENGTH_cv*block)] %warto�ci BER dla danych EbNo
% BER_SOFT = [BER_SOFT total_VB_Err/(LENGTH_cv*block)] %warto�ci BER dla danych EbNo

% fprintf('\nSimulated Block = %d \n',block); %numer bloku
fprintf('Decoding error (HARD)  = %d \n', total_VB_Err_HARD); %ilosc bled�w w danym bloku
fprintf('Decoding error (SOFT) = %d \n',total_VB_Err);


number_of_error_hard
BER_hard = number_of_error_hard/LENGTH_cv

number_of_error_soft
BER_soft = number_of_error_soft/LENGTH_cv



fclose(id);


